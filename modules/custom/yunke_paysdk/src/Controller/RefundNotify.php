<?php

/**
 *  接收来自统一平台的退款异步通知
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_paysdk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\yunke_paysdk\Controller
 */
class RefundNotify extends ControllerBase {


  public function __construct() {

  }


  /**
   * 处理统一平台发来的退款异步通知
   *
   * @param null $orderNumber
   */
  public function index() {
    //检查订单号、退款单号
    //验证签名
    //变更订单状态
    //处理和订单相关的后续业务逻辑，但这也可以在查询退款逻辑中处理
    return new Response('success');
  }


}

