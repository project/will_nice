<?php

/**
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_paysdk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;


/**
 * 自定义控制器
 * 你可以用这个类来执行一些自定义控制器代码
 * 即使是在维护模式下，该控制器也可以访问的
 *
 * @package Drupal\yunke_paysdk\Controller
 */
class Test extends ControllerBase {


  public function __construct() {

  }


  /**
   * 提供自定义控制器代码运行
   *
   * @param Request $request
   *
   * @return array
   */
  public function test(Request $request) {

    echo "<pre>";
    echo "请先在以下位置自定义需要执行的PHP代码：\n";
    echo "文件：" . __FILE__ . "\n";
    echo "方法：" . __METHOD__ . "\n";
    die;

  }


}
