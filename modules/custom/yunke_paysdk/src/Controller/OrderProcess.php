<?php

/**
 *  在统一平台订单付款完成后将返回商家，这里处理和订单相关的后续业务逻辑
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_paysdk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\yunke_paysdk\Controller
 */
class OrderProcess extends ControllerBase {


  public function __construct() {

  }


  /**
   * 处理统一平台发来的支付状态异步通知
   *
   * @param null $orderNumber
   */
  public function index($orderNumber = NULL) {
    //检查订单是否属于当前用户
    //检查是否有异步通知变更订单状态，如果有变更则可以信任变更
    //如果异步通知尚未发生，那么应该主动发起订单查询以确认订单状态
    //确认订单后，处理和订单相关的后续业务逻辑，但这也可以在订单异步通知中处理
    //系统应该标明是否已经处理，以及在哪种情况下进行了处理
    $msg = '欢迎回到客户端系统，订单号：' . $orderNumber;
    //return new Response($msg);
    return [
      '#markup' => $msg,
    ];

  }


}

