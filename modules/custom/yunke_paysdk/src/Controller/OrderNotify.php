<?php

/**
 *  接收来自统一平台的异步订单通知
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_paysdk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\yunke_paysdk\Controller
 */
class OrderNotify extends ControllerBase {


  public function __construct() {

  }


  /**
   * 处理统一平台发来的支付状态异步通知
   *
   * @param null $orderNumber
   */
  public function index($orderNumber = NULL) {
    //检查订单号与发送回来的是否匹配
    //验证签名
    //变更订单状态
    //处理和订单相关的后续业务逻辑，但这也可以在返回链接中处理
    //在时间上返回链接的执行和异步通知并无确定的先后顺序，两者都可以处理业务，系统依据情况而定
    return new Response('success');
  }


}

