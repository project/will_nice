<?php
/**
 * 商家下单付款示例
 *
 */

namespace Drupal\yunke_paysdk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use \Drupal\Core\Routing\TrustedRedirectResponse;

class OrderForm extends FormBase {

  public function getFormId() {
    return 'yunke_paysdk_order_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $notice = '本页向你展示客户端系统如何通过统一收银平台下单付款<br>';
    $notice .= '你可以输入小笔金额进行真实付款，然后在退款演示页凭订单号进行退款，请注意超期不退款、不开票，这仅做测试';
    $form['notice'] = [
      '#markup' => $notice,
    ];

    $form['description'] = [
      '#type'        => 'textfield',
      '#title'       => '订单描述',
      '#description' => '用于描述商品或服务，不超过127个字符，不可使用特殊字符，如 /，=，& 等',
      '#required'    => TRUE,
      '#maxlength'   => 127,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['total'] = [
      '#type'         => 'number',
      '#title'        => '订单金额',
      '#description'  => '单位：元，取值范围为 0.01~100000000.00',
      '#required'     => TRUE,
      '#min'          => 0.01,
      '#max'          => 100000000.00,
      '#step'         => 0.01,
      '#field_suffix' => '元',
      '#attributes'   => [
        'autocomplete' => 'off',
      ],
    ];

    //订单超时时间应该由系统生成，不应由用户输入，为演示起见，这里展示给用户
    $form['timeout_express'] = [
      '#type'          => 'datetime',
      '#title'         => '订单关闭时间',
      '#description'   => '超过该时间交易将关闭，无法进行付款，订单有效期范围在1分钟到15天',
      '#required'      => TRUE,
      '#default_value' => DrupalDateTime::createFromTimestamp(time() + 7 * 24 * 60 * 60),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $order_number = 'yunkesdk_' . date('ymdHis') . mt_rand(10000000, 99999999);
    //订单号应由系统按一定规则生成，而不是用户指定，为演示起见，这里展示给用户
    $form['order_number'] = [
      '#type'          => 'textfield',
      '#title'         => '订单号',
      '#description'   => '客户端系统订单号，6-32个字符，仅限大小写字母、数字、下划线，在客户端系统中应保证全局唯一',
      '#maxlength'     => 32,
      '#pattern'       => '^[0-9a-zA-Z_]{6,32}$',
      '#required'      => TRUE,
      '#default_value' => $order_number,
      '#attributes'    => [
        'autocomplete' => 'off',
        //'readonly'     => TRUE,
      ],
    ];


    $form['attach'] = [
      '#type'  => 'value',
      '#value' => '',
      //发送给统一付款平台的订单附加数据，在异步通知或返回时将原样送回
      //长度string[1,128] 任意数据，推荐为json字符串
      //内部信息不展示给用户
    ];


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '生成订单并跳转至统一平台付款',
      '#button_type' => 'primary',
    ];
    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['description'] = trim($form_state->getValue('description'));
    $order['total'] = trim($form_state->getValue('total'));
    $order['timeout_express'] = $form_state->getValue($form['timeout_express']['#parents'])->getTimestamp();
    $order['order_number'] = trim($form_state->getValue('order_number'));
    $order['attach'] = trim($form_state->getValue('attach'));

    //付款后跳回本系统的绝对URL地址，你应该在该地址或异步通知中处理付款后的后续业务逻辑
    //string[1,256] 必须为直接可访问的URL绝对地址
    //内部信息不展示给用户
    $route_parameters = ['orderNumber' => $order['order_number'],];
    $options = ['absolute' => TRUE,];
    $return_url = new Url('yunke_paysdk.process', $route_parameters, $options);
    $order['return_url'] = $return_url->toString(FALSE);

    //付款状态异步通知链接，统一收银平台将采用该链接向本系统发送付款是否成功的消息
    //在该地址中你应该处理订单变更逻辑
    //string[1,256] 必须为直接可访问的URL绝对地址
    //内部信息不展示给用户
    $route_parameters = ['orderNumber' => $order['order_number'],];
    $options = ['absolute' => TRUE,];
    $notify_url = new Url('yunke_paysdk.notify', $route_parameters, $options);
    $order['notify_url'] = $notify_url->toString(FALSE);


    $sdk = \Drupal::service('yunke_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order);
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->order($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //直接跳转到统一支付平台选择付款界面
    $result = $form_state->get('result');
    $form_state->setResponse(new TrustedRedirectResponse($result['url']));
  }

}
