<?php
/**
 * 商家订单查询示例
 *
 */

namespace Drupal\yunke_paysdk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class QueryForm extends FormBase {

  public function getFormId() {
    return 'yunke_paysdk_query_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $notice = '本页向你展示客户端系统如何向统一收银平台查询订单付款的状态<br>';
    $notice .= '你需要先通过付款演示页提交一个订单，并记下订单号（注：客户端系统订单号，非统一平台系统订单号）';
    $form['notice'] = [
      '#markup' => $notice,
    ];


    $form['order_number'] = [
      '#type'        => 'textfield',
      '#title'       => '订单号',
      '#description' => '请输入你在付款演示中提交的订单号',
      '#maxlength'   => 32,
      '#required'    => TRUE,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '查询',
      '#button_type' => 'primary',
      '#ajax'        => [
        'callback' => '::query',
        'wrapper'  => 'query-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => '正在进行查询...',
        ],
      ],
    ];

    $form['content_one'] = [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#value'      => '点击查询后结果将显示在这里',
      '#attributes' => ['id' => 'query-result-wrapper'],
    ];


    return $form;

  }

  public function query(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = '已查询';
    if (isset($result['order_state'])) {
      // 0|等待付款、1|成功付款、2|付款失败、3|退款中、4|部分退款、5|全部退款
      $state = [0 => '等待付款', 1 => '成功付款', 2 => '付款失败', 3 => '退款中', 4 => '部分退款', 5 => '全部退款'];
      $title = '订单付款状态：<strong>' . $state[(int) $result['order_state']] . '</strong>';
    }

    $return = [];
    $return['order_info'] = [
      '#type'  => 'details',
      '#title' => $title,
      '#open'  => TRUE,
    ];
    $data = '统一平台返回数据：<pre>' . print_r($result, TRUE) . "</pre>";
    $return['order_info']['data'] = ['#markup' => $data];
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));

    $sdk = \Drupal::service('yunke_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'query');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->query($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
  }

}
