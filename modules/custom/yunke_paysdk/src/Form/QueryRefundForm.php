<?php
/**
 * 商家订单查询退款示例
 *
 */

namespace Drupal\yunke_paysdk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class QueryRefundForm extends FormBase {

  public function getFormId() {
    return 'yunke_paysdk_query_refund_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $notice = '本页向你展示客户端系统如何向统一收银平台查询退款订单的退款状态<br>';
    $notice .= '你需要先通过付款演示页提交一个订单进行真实付款，然后通过退款页发起退款，并记下订单号和退款单号';
    $form['notice'] = [
      '#markup' => $notice,
    ];


    $form['order_number'] = [
      '#type'        => 'textfield',
      '#title'       => '订单号',
      '#description' => '请输入你在付款演示中提交的订单号',
      '#maxlength'   => 32,
      //'#pattern'     => '^[0-9a-zA-Z_]{6,32}$',
      '#required'    => TRUE,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['refund_number'] = [
      '#type'        => 'textfield',
      '#title'       => '退款单号',
      '#description' => '请输入你在退款演示页中输入的退款单号',
      '#maxlength'   => 32,
      //'#pattern'     => '^[0-9a-zA-Z_]{6,32}$',
      '#required'    => TRUE,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '查询退款',
      '#button_type' => 'primary',
      '#ajax'        => [
        'callback' => '::refund',
        'wrapper'  => 'refund-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => '正在查询退款...',
        ],
      ],
    ];

    $form['content_one'] = [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#value'      => '查询后退款结果将显示在这里',
      '#attributes' => ['id' => 'refund-result-wrapper'],
    ];


    return $form;

  }

  public function refund(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = '已提交退款查询';
    if (isset($result['refund_state'])) {
      $state = [0 => '等待退款', 1 => '退款成功', 2 => '退款失败', ];
      $title = '退款状态：<strong>' . $state[(int) $result['refund_state']] . '</strong>';
    }

    $return = [];
    $return['refund_info'] = [
      '#type'  => 'details',
      '#title' => $title,
      '#open'  => TRUE,
    ];
    $data = '统一平台返回数据：<pre>' . print_r($result, TRUE) . "</pre>";
    $return['refund_info']['data'] = ['#markup' => $data];
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));
    $order['refund_number'] = trim($form_state->getValue('refund_number'));

    $sdk = \Drupal::service('yunke_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'query_refund');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->queryRefund($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
  }

}
