<?php
/**
 * 商家订单退款示例
 *
 */

namespace Drupal\yunke_paysdk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class RefundForm extends FormBase {

  public function getFormId() {
    return 'yunke_paysdk_refund_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $notice = '本页向你展示客户端系统如何向统一收银平台发起退款<br>';
    $notice .= '你需要先通过付款演示页提交一个订单进行真实付款，并记下订单号';
    $form['notice'] = [
      '#markup' => $notice,
    ];


    $form['order_number'] = [
      '#type'        => 'textfield',
      '#title'       => '订单号',
      '#description' => '请输入你在付款演示中提交的订单号',
      '#maxlength'   => 32,
      '#required'    => TRUE,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['refund_number'] = [
      '#type'        => 'textfield',
      '#title'       => '退款单号',
      '#description' => '一个订单可分多次退款，以便于订单内部分商品退款或用户补偿，采用退款单号唯一标识每一次退款，相同退款单号指同一个退款申请，输入格式与订单号相同，不同批次请采用不同退款单号',
      '#maxlength'   => 32,
      '#required'    => TRUE,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    $form['amount'] = [
      '#type'         => 'number',
      '#title'        => '退款金额',
      '#description'  => '单位：元，取值范围为 0.01~100000000.00，值大于0，小于等于订单总金额，如多次退款，则所有退款总额不能大于订单付款总金额',
      '#required'     => TRUE,
      '#min'          => 0.01,
      '#max'          => 100000000.00,
      '#step'         => 0.01,
      '#field_suffix' => '元',
      '#attributes'   => [
        'autocomplete' => 'off',
      ],
    ];

    $form['reason'] = [
      '#type'        => 'textfield',
      '#title'       => '退款原因',
      '#description' => '可选，若传入，会在下发给用户的退款消息中体现退款原因，80个字符以内',
      '#maxlength'   => 80,
      '#attributes'  => [
        'autocomplete' => 'off',
      ],
    ];

    //退款状态异步通知链接
    $route_parameters = [];
    $options = ['absolute' => TRUE,];
    $notify_url = new Url('yunke_paysdk.refund.notify', $route_parameters, $options);
    $form['notify_url'] = [
      '#type'  => 'value',
      '#value' => $notify_url->toString(FALSE),
      //退款异步通知链接，统一收银平台将采用该链接向本系统发送退款是否成功的消息
      //在该地址中你应该处理订单变更逻辑  或者在主动查询退款后处理
      //string[1,256] 必须为直接可访问的URL绝对地址
      //内部信息不展示给用户
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '退款',
      '#button_type' => 'primary',
      '#ajax'        => [
        'callback' => '::refund',
        'wrapper'  => 'refund-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => '正在提交退款...',
        ],
      ],
    ];

    $form['content_one'] = [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#value'      => '提交后退款处理结果将显示在这里',
      '#attributes' => ['id' => 'refund-result-wrapper'],
    ];


    return $form;

  }

  public function refund(array &$form, FormStateInterface $form_state) {
    $result = $form_state->get('result');
    unset($result['sign']);
    $title = '已提交退款';
    if (isset($result['refund_state'])) {
      $state = [0 => '等待退款', 1 => '退款成功', 2 => '退款失败',];
      $title = '退款状态：<strong>' . $state[(int) $result['refund_state']] . '</strong>';
    }

    $return = [];
    $return['refund_info'] = [
      '#type'  => 'details',
      '#title' => $title,
      '#open'  => TRUE,
    ];
    $data = '统一平台返回数据：<pre>' . print_r($result, TRUE) . "</pre>";
    $return['refund_info']['data'] = ['#markup' => $data];
    return $return;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $order['order_number'] = trim($form_state->getValue('order_number'));
    $order['refund_number'] = trim($form_state->getValue('refund_number'));
    $order['amount'] = $form_state->getValue('amount');
    $order['reason'] = trim($form_state->getValue('reason'));
    $order['notify_url'] = trim($form_state->getValue('notify_url'));

    $sdk = \Drupal::service('yunke_paysdk.pay');
    $verifyResult = $sdk->verifyParameters($order, 'refund');
    if ($verifyResult !== TRUE) {
      $form_state->setError($form, $verifyResult);
    }
    $result = $sdk->refund($order);
    if ($result['code'] >= 4000) {
      $form_state->setError($form, $result['msg']);
    }
    $form_state->set('result', $result);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //表单的AJAX提交中，验证器通过后提交器会执行 ，最后再执行ajax回调
  }

}
