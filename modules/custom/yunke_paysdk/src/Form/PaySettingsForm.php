<?php
/**
 * 模块设置表单
 *
 */

namespace Drupal\yunke_paysdk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class PaySettingsForm extends FormBase {

  public function getFormId() {
    return 'yunke_paysdk_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#sdkConfig'] = $this->config('yunke_paysdk.settings');
    $notice='设置统一付款平台接口信息，在正确设置后，才能使用付款功能';
    $form['notice'] = [
      '#markup' => $notice,
    ];
    $form['userID'] = [
      '#type'          => 'textfield',
      '#title'         => '用户ID',
      '#description'   => '在统一付款平台注册的用户ID',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('userID'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['key'] = [
      '#type'          => 'textfield',
      '#title'         => '通讯密钥',
      '#description'   => '与统一付款平台进行通讯的密钥，必须与平台同步设置，否则将不能通讯成功',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('key'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiOrderURL'] = [
      '#type'          => 'textfield',
      '#title'         => '下单接口地址',
      '#description'   => '系统将通过该接口获取用户实际下单付款的地址',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('api.orderURL'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiQueryURL'] = [
      '#type'          => 'textfield',
      '#title'         => '查询接口地址',
      '#description'   => '系统将通过该接口获取用户实际下单订单的状态',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('api.queryURL'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiRefundURL'] = [
      '#type'          => 'textfield',
      '#title'         => '退款接口地址',
      '#description'   => '系统将通过该接口进行退款操作',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('api.refundURL'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['apiQueryRefundURL'] = [
      '#type'          => 'textfield',
      '#title'         => '退款查询接口地址',
      '#description'   => '系统将通过该接口进行退款状态查询操作',
      '#required'      => TRUE,
      '#default_value' => $form['#sdkConfig']->get('api.queryRefundURL'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Submit'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    //进行配置正确性验证
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('yunke_paysdk.settings');
    $config->set('userID', $form_state->getValue('userID'));
    $config->set('key', $form_state->getValue('key'));
    $config->set('api.orderURL', $form_state->getValue('apiOrderURL'));
    $config->set('api.queryURL', $form_state->getValue('apiQueryURL'));
    $config->set('api.refundURL', $form_state->getValue('apiRefundURL'));
    $config->set('api.queryRefundURL', $form_state->getValue('apiQueryRefundURL'));
    $config->save();
    $this->messenger()->addStatus('设置成功');
  }

}

