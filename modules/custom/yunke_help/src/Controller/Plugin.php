<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180705
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * 自定义控制器
 * 进行插件相关操作
 * 即使是在维护模式下，该控制器也可以访问的
 * Class Test
 *
 * @package Drupal\yunke_help\Controller
 */
class Plugin extends ControllerBase
{


    public function __construct()
    {

    }

    /**
     * 提供插件管理器相关操作
     *
     * @param Request $request
     *
     * @return array
     */
    public function index(Request $request)
    {
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        $post = $request->request->all();
        if (isset($post['clearCache'])) {
            $this->clearCache($post);
        } elseif (isset($post['showPlugins'])) {
            $this->showPlugins($post);
        } else {
            echo "错误!";
        }
        echo "\n</pre>";
        die;


    }

    /**
     * 清理各插件管理器插件定义缓存
     *
     * @param $post
     */
    public function clearCache($post)
    {
        $pluginManagerId = $post['pluginManagerId']; //未进行任何安全处理,因此不要用于生产站点
        $container = \Drupal::getContainer();
        if ($container->has($pluginManagerId)) {
            $pluginManager = \Drupal::service($pluginManagerId);
        } else {
            echo "错误:不存在该插件管理器";
            return;
        }
        if ($pluginManager instanceof \Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface) {
            $pluginManager->clearCachedDefinitions();
            echo "插件管理器：" . $pluginManagerId . "的插件定义缓存已被清理";
        } else {
            echo "插件管理器：" . $pluginManagerId . "没有实现可缓存发现接口，插件定义无缓存或需通过重建刷新";
        }
    }

    /**
     * 查看插件定义
     *
     * @param $post
     */
    public function showPlugins($post)
    {
        $pluginManagerId = $post['pluginManagerId']; //未进行任何安全处理,因此不要用于生产站点
        $container = \Drupal::getContainer();
        if ($container->has($pluginManagerId)) {
            $pluginManager = \Drupal::service($pluginManagerId);
        } else {
            echo "错误:不存在该插件管理器";
            return;
        }
        if (!($pluginManager instanceof \Drupal\Component\Plugin\Discovery\DiscoveryInterface)) {
            echo "错误:该服务不是插件管理器";
            return;
        }
        $definitions = $pluginManager->getDefinitions();
        echo "插件管理器：" . $pluginManagerId . "有以下插件：\n";
        print_r(array_keys($definitions));
        echo "\n插件定义如下：\n";
        print_r($definitions); //部分定义是使用对象方式  打印时可能存在内存超限 需要改进
    }

}

