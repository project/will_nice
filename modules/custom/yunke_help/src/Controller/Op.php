<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20190523/20200320
 */

namespace Drupal\yunke_help\Controller;


/**
 * Class Op
 *
 * @package Drupal\yunke_help\Controller
 */
class Op
{
  /**
   * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
   *
   * @var string
   */
  protected $root;

  /**
   * 本模块相对于系统根目录的路径，不带前后缀斜杠
   *
   * @var string
   */
  protected $yunke_help_path;


  public function __construct()
  {
    //$this->root = \Drupal::service("app.root");
    //$this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
  }

  /**
   * 中转操作动作
   */
  public function index($type = null)
  {
    if (empty($type)) {
      echo "链接不正确";
      die;
    }
    switch ($type) {
      case "locale-rebuild-js":
        $this->locale_rebuild_js();
        break;
      case "reparse-all-locale-js-file":
        $this->reparse_all_locale_js_file();
        break;
      case "delete-unused-managed-file":
        $this->delete_unused_managed_file();
        break;
      case "cron-key":
        $this->cron_key();
        break;
      case "clean-batch":
        $this->clean_batch();
        break;
      case "rebuild-routes":
        return $this->rebuildRoutes();
      case "rebuild-theme-registry":
        return $this->rebuildThemeRegistry();
      default:
        echo "链接不正确";
    }
    die;

  }

  /**
   * 刷新主题钩子注册数据，这会让系统重新扫描主题
   * @return array
   */
  public function rebuildThemeRegistry(){
    \Drupal::service('theme.registry')->reset();
    return [
      '#markup' => '主题钩子注册数据已重置刷新',
      '#title'  => '执行结果：',
    ];
  }

  /**
   * 重建系统全部路由，相当于删除系统路由表，然后重新收集路由数据后再插入
   * 当路由数据表损坏 出现404页面或异常页面时请使用该功能
   * @return array
   */
  public function rebuildRoutes()
  {
    \Drupal::service('router.builder')->setRebuildNeeded();
    return [
      '#markup' => '全部路由已重建',
      '#title'  => '执行结果：',
    ];
  }

  /**
   * 清理残留批设置数据
   */
  public function clean_batch()
  {
    $db = \Drupal::database();
    $table = 'batch';
    $msg = '';
    if (!$db->schema()->tableExists($table)) {
      $msg = '批设置储存数据表尚未建立';
    } else {
      $select = $db->select($table);
      $select->addField($table, 'bid');
      $ids = $select->execute()->fetchCol();
      if (empty($ids)) {
        $msg = "无残留\n";
      } else {
        $msg = '残留批设置的批id：' . implode(', ', $ids) . "\n";
      }
      $db->truncate($table)->execute();
      //批回调队列中的数据不必处理，计划任务会自动清理，见system_cron()
      //通常批处理有残余说明系统有任务被漏执行，用户在可能的情况下应该继续执行
      $msg .= '已清理';
    }
    echo "<pre>\n";
    echo "Drupal版本号：" . \Drupal::VERSION . "   \n";
    echo "清理残余批处理设置数据情况如下：\n\n";
    echo $msg;
    echo "\n\nby:yunke_help模块\n";
    echo "</pre>\n";
    die;
  }

  /**
   * 修改计划任务外部URL触发方式所用的密钥
   */
  public function cron_key()
  {
    $key = \Drupal::state()->get('system.cron_key');
    $cron_key = \Drupal\Component\Utility\Crypt::randomBytesBase64(55);
    \Drupal::state()->set('system.cron_key', $cron_key);
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 操作者:yunke_help模块\n";
    echo "计划任务外部URL触发方式所用的密钥已被修改为：\n";
    echo $cron_key . "\n";
    echo "之前的密钥为：\n";
    echo $key;
    echo "</pre>";
  }

  /**
   * 仅删除通过托管文件API(ManagedFile API)上传的文件,
   * 不管处于临时还是永久状态，只要使用次数为0即会被删除，慎用！临时文件会被自动删除！
   */
  public function delete_unused_managed_file()
  {
    set_time_limit(0);
    if (!\Drupal::moduleHandler()->moduleExists('file')) {
      echo "文件模块不存在或未启用";
      die;
    }

    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $db = \Drupal::database();
    //先找出有追踪记录的全部文件
    $fids = $db->select('file_usage')
      ->fields('file_usage', ['fid'])
      ->execute()
      ->fetchAll(\PDO::FETCH_COLUMN, 0);
    //找出没有追踪记录的全部文件，这些文件一定没有被使用
    $unrecordedUnusedManagedFiles = \Drupal::entityQuery('file')
      ->condition('fid', $fids, 'NOT IN')
      ->execute();
    //找出全部使用次数降为0的文件，它们没有被使用
    $recordedUnusedManagedFiles = $db->select('file_usage')
      ->fields('file_usage', ['fid'])
      ->condition('count', 0, '=')
      ->execute()
      ->fetchAll(\PDO::FETCH_COLUMN, 0);
    //计算全部未使用文件，而不管其状态为何
    $unusedManagedFiles = array_unique(array_merge($unrecordedUnusedManagedFiles, $recordedUnusedManagedFiles));
    $unusedManagedFilesEntities = $file_storage->loadMultiple($unusedManagedFiles);
    $msg = '';
    foreach ($unusedManagedFilesEntities as $file) {
      $msg .= "(id:" . $file->id() . ") " . $file->getFilename();
      try {
        $status = "  已被删除\n";
        $file->delete();
      } catch (\Exception $exception) {
        $status = "  删除异常:\n" . $exception->getMessage() . "\n\n";
      }
      $msg .= $status;
    }
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 操作者:yunke_help模块\n";
    if (empty($unusedManagedFilesEntities)) {
      echo "当前系统没有未被使用的托管文件\n";
    } else {
      echo "共" . count($unusedManagedFilesEntities) . "个文件未被使用，删除执行如下：\n" . $msg;
    }
  }

  /**
   * 重建js翻译文件
   */
  public function locale_rebuild_js()
  {
    $dir = 'public://' . \Drupal::config('locale.settings')->get('javascript.directory');
    $js_files = [];
    $languages = locale_translatable_language_list();
    foreach ($languages as $lcode => $data) {
      if (_locale_rebuild_js($lcode)) {
        $locale_javascripts = \Drupal::state()->get('locale.translation.javascript') ?: [];
        if (!empty($locale_javascripts[$lcode])) {
          $js_files[$lcode] = $dir . '/' . $lcode . '_' . $locale_javascripts[$lcode] . '.js';
        }
      }
    }
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 操作者:yunke_help模块\n";
    if (!empty($js_files)) {
      echo "\njs翻译数据重建完成！\n";
      echo "\n文件如下：\n";
      print_r($js_files);
    } else {
      echo "\njs翻译数据无需重建，或者重建时发生错误\n";
    }
    echo "</pre>";
  }

  /**
   * 重新提取所有已加载的本地js文件中的翻译
   */
  public function reparse_all_locale_js_file()
  {
    $parsed = \Drupal::state()->get('system.javascript_parsed') ?: [];
    \Drupal::state()->set('system.javascript_parsed', []);
    foreach ($parsed as $k => $v) {
      if (!is_numeric($k)) {
        unset($parsed[$k]);
      }
    }

    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 操作者:yunke_help模块\n";
    echo "设置成功！\n所有曾加载过的本地js文件将在后续请求中被重新提取翻译\n提取翻译的意思是：js文件中所有Drupal.t和Drupal.formatPlural方法的参数将被正则解析提取，并保存到数据库中的翻译相关数据表，提取后管理员可以在后台为其添加翻译\n";
    echo "\n曾提取过翻译的js文件如下：\n";
    print_r($parsed);
    echo "</pre>";
  }


  /**
   * @param null  $name    打印数据的名称
   * @param array $data    数据内容
   * @param bool  $showKey 是否显示键名
   */
  protected function showData($name = null, $data = array(), $showKey = false)
  {
    echo "<pre>";
    echo "Drupal版本号：" . \Drupal::VERSION . " 操作者:yunke_help模块\n";
    echo "以下是" . $name . "数据：\n";
    if ($showKey) {
      $dataKey = array_keys($data);
      echo "共有 " . count($dataKey) . " 项:\n";
      print_r($dataKey);
    }
    echo "\n数据如下：\n";
    print_r($data);
    echo "</pre>";
  }


}

