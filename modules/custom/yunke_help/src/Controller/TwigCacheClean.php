<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180628
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\PhpStorage\PhpStorageFactory;

/**
 * 自定义控制器
 * 清理twig编译后的缓存文件
 *
 * @package Drupal\yunke_help\Controller
 */
class TwigCacheClean
{

    public function clean()
    {
        $storage = PhpStorageFactory::get('twig');

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . "   \n\n";

        if ($storage->deleteAll()) {
            echo "已删除全部twig模板缓存，包括有效缓存\n";
        } else {
            echo "删除失败，请检查权限\n";
        }

        echo "\n缓存清空操作已完成 by:yunke_help模块\n";
        echo "</pre>\n";
        die;

    }

    public function garbageCollection()
    {
        $storage = PhpStorageFactory::get('twig');
        $storage->garbageCollection();
        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . "   \n\n";
        echo "已删除全部不再使用的twig模板缓存，有效缓存不被删除\n";
        echo "\n缓存清空操作已完成 by:yunke_help模块\n";
        echo "</pre>\n";
        die;
    }

}

