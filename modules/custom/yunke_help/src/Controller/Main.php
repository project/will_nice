<?php
/**
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20180610
 */

namespace Drupal\yunke_help\Controller;

use Drupal\Core\Serialization\Yaml;

class Main
{
    /**
     * 系统根目录的绝对路径 不带后缀斜杠 如：C:\root\drupal
     *
     * @var string
     */
    protected $root;

    /**
     * 本模块相对于系统根目录的路径，不带前后缀斜杠
     *
     * @var string
     */
    protected $yunke_help_path;


    public function __construct()
    {
        //$this->root = \Drupal::service("app.root");
        //$this->yunke_help_path = \Drupal::moduleHandler()->getModule("yunke_help")->getPath();
    }

    /**
     * 输出本模块使用首页
     */
    public function index()
    {
        $container = \Drupal::getContainer();
        $serviceIds = $container->getServiceIds();
        $pluginManagerIds = [];
        foreach ($serviceIds as $id) {
            if (stripos($id, 'plugin.manager.') === 0) {
                $pluginManagerIds[] = $id;
            }
        }

        //不规则命名插件管理器服务id
        $pluginIds = [
            'validation.constraint',
            'typed_data_manager',
            'image.toolkit.manager',
            'image.toolkit.operation.manager',
        ];
        $pluginManagerIds = array_merge($pluginManagerIds, $pluginIds);

        $info = Yaml::decode(file_get_contents(\Drupal::moduleHandler()->getModule("yunke_help")->getPathname()));

        $variables = [];
        $variables["Drupal_VERSION"] = ['#markup' => \Drupal::VERSION]; //此处这样写是因为子元素需要是一个渲染数组，否则会有错误提示
        $variables["VERSION"] = ['#markup' => $info["version"]];
        $variables["pluginManagerIds"] = $pluginManagerIds; //此处之所以可以这样写是因为没有该子元素的渲染流程，只需要是数组即可
        //$variables["pluginManagerForm"]=\Drupal::formBuilder()->getForm("\Drupal\yunke_help\Form\PluginManager");
        $variables['#theme'] = "yunke_help_index";
        $variables['#type'] = "page";
        $variables['#title'] = "yunke_help学习辅助模块";
        return $variables;
    }

}

