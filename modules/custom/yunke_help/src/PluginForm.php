<?php
/**
 * @file
 * 创建子表单
 */

namespace Drupal\yunke_help;

use Drupal\Core\Form\FormStateInterface;

/**
 * 向某个父表单添加子表单
 */
class PluginForm
{

    /**
     * 构建子表单
     */
    public function buildForm(array &$form, FormStateInterface $form_state)
    {
        $form['#tree'] = true; //必须设置，很重要
        $form['a']['b']['c1'] = array(
            '#type'  => 'textfield',
            '#title' => "子表单字段1",
            '#size'  => 100,
        );
        $form['a']['b']['c2'] = array(
            '#type'  => 'textfield',
            '#title' => "子表单字段2",
            '#size'  => 100,
        );
        /*
        $form['s'] = array(
            '#type'  => 'textfield',
            '#title' => "全局值",
            '#size'  => 100,
            '#tree'=>false, //因为这样的设置，该字段的值无法通过子表单状态对象获取
        );
        */
        return $form;
    }

    /**
     * 验证子表单
     */
    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValue(['a', 'b', 'c1']))) {
            $form_state->setErrorByName(implode('][', ['a', 'b', 'c1']), "子表单：字段1值不能为空");
        }
    }

    /**
     * 提交子表单
     */
    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $value = "子表单值：" . $form_state->getValue(['a', 'b', 'c1']);
        drupal_set_message($value);
    }

}
