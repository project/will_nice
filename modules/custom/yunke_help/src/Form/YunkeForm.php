<?php
/**
 * 演示表单对象属性取值赋值问题
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class YunkeForm extends FormBase {

  protected $name = NULL;

  protected $company = NULL;

  public function getFormId() {
    return 'yunke_help_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->name = 'yunke';
    $form['name'] = [
      '#type'          => 'textfield',
      '#title'         => 'name',
      '#default_value' => $this->name,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];
    $form['view'] = [
      '#type'  => 'button',
      '#value' => 'view',
      '#ajax'  => [
        'callback' => '::report',
        'wrapper'  => 'report-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => 'load...',
        ],
      ],
    ];
    $form['view2'] = [
      '#type'  => 'submit',
      '#value' => 'submit',
      '#ajax'  => [
        'callback' => '::report',
        'wrapper'  => 'report-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => 'load...',
        ],
      ],
    ];

    $form['content'] = [
      '#type'       => 'container',
      '#optional'   => FALSE,
      '#attributes' => ['id' => 'report-result-wrapper'],
    ];
    return $form;
  }

  public function report(array &$form, FormStateInterface $form_state) {
    return [
      '#markup' => 'report:' . $this->name . ' ' . $this->company,
    ];
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->name = $form_state->getValue('name');
    $this->company = 'will-nice';
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addStatus('submit(name):' . $this->name);
  }

}
