<?php

/**
 * 构建表单，提交类的全名字空间名得到类定义文件路径
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ClassPath extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_ClassPath';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['class_name'] = array(
            '#type'  => 'textfield',
            '#title' => "输入全限定名字空间类名：",
            '#size'  => 100,
            '#required' => TRUE,
        );
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => "提交",
            '#button_type' => 'primary',
        );
        $form['actions']['reset'] = array(
            '#type'       => 'html_tag',
            '#tag'        => 'input',
            '#attributes' => ['type' => 'reset', 'value' => "重置", 'class' => "button"],
        );
        $form['#attributes']["target"] = "_blank";
        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $class_name = $form_state->getValue('class_name');
        if (empty(trim($class_name))) {
            $form_state->setErrorByName('class_name', "类名不能为空");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $class = $form_state->getValue('class_name');
        $class = trim($class, '\\');
        
        $loader = \Drupal::service("class_loader");

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "当前指定的类名是：\n" . $class . "\n\n";
        if ($file = $loader->findFile($class)) {
            $file = realpath($file);
            echo "被定义在以下文件中：\n";
            echo $file;
        } else {
            echo "不能为其找到定义文件，你需要提供一个完全限定名字空间类名，并确保已经在系统中定义";
        }
        echo "\n</pre>";
        die;
    }

}
