<?php

/**
 * 显示语言配置覆写数据
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


class LanguageConfigOverride extends FormBase
{

    public function getFormId()
    {
        return 'yunke_help_language_config_override';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $languages = \Drupal::service("language_manager")->getLanguages();
        $langcode = [];
        foreach ($languages as $language) {
            $langcode[$language->getId()] = $language->getId();
        }


        $form['description'] = [
            '#markup' => '选择一个语言，然后键入配置对象名（配置项名或配置文件名），查看其在该语言下的覆写数据'
        ];
        $form['langcode'] = [
            '#type'         => 'select',
            '#options'      => $langcode,
            '#empty_option' => $this->t('-select-'),
            '#required'     => TRUE,
            '#field_prefix' => '选择语言：',
        ];
        $form['name'] = array(
            '#type'     => 'textfield',
            '#title'    => "配置对象名：",
            '#size'     => 60,
            '#required' => TRUE,
        );
        $form['actions'] = [
            '#type' => 'actions',
        ];
        $form['actions']['submit'] = array(
            '#type'  => 'submit',
            '#value' => '查看',
        );
        $form['actions']['reset'] = [
            '#type'        => 'button',
            '#button_type' => 'reset',
            '#value'       => $this->t('Reset'),
            '#attributes'  => [
                'onclick' => 'this.form.reset(); return false;',
            ],
        ];
        $form['#attributes']['target'] = "_blank";
        $form['#attached']['library'][] = 'yunke_help/removeFormSingleSubmit';
        $form['#title'] = "显示语言覆写信息";

        return $form;
    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        $name = $form_state->getValue('name');
        if (empty(trim($name))) {
            $form_state->setErrorByName('name', "请输入配置对象名");
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $langcode = $form_state->getValue('langcode');
        $name = $form_state->getValue('name');

        $config_factory_override = \Drupal::service("language.config_factory_override");
        $override = $config_factory_override->getOverride($langcode, $name)->get();
        if (empty($override)) {
            $override = "不存在覆写数据";
        }
        $config = \Drupal::configFactory()->getEditable($name)->get();
        $configLangcode = isset($config['langcode']) && (!empty($config['langcode'])) ? $config['langcode'] : 'en';
        if (empty($config)) {
            $config = "不存在该配置项";
            $configLangcode = "不存在";
        }

        $iniLanguage = \Drupal::configFactory()->get('system.site')->get('langcode') ?: 'en';
        $defaultLangcode = \Drupal::configFactory()->get('system.site')->get('default_langcode') ?: 'en';

        echo "<pre>\n";
        echo "Drupal版本号：" . \Drupal::VERSION . " 导出者:yunke_help模块\n";
        echo "系统安装时选定的语言是：" . $iniLanguage . "\n";
        echo "系统当前选定默认语言是：" . $defaultLangcode . "\n\n";
        echo "在语言{$langcode}下的覆写数据如下：\n";
        print_r($override);
        echo "\n\n源配置使用语言：{$configLangcode} 配置数据如下:\n";
        print_r($config);
        echo "\n</pre>";
        die;
    }


}
