<?php
/**
 * 演示表单对象属性取值赋值问题
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class YunkeForm extends FormBase {

  protected $title = NULL;

  protected $user = NULL;
  

  public function getFormId() {
    return 'yunke_help_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->title = 'yunke';
    $form['title'] = [
      '#type'          => 'textfield',
      '#title'         => 'title',
      '#default_value' => $this->title,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];
    $form['view'] = [
      '#type'                    => 'button',
      '#value'                   => 'view',
      '#ajax'                    => [
        'callback' => '::report',
        'wrapper'  => 'report-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => 'load...',
        ],
      ],
    ];

    $form['content'] = [
      '#type'       => 'container',
      '#optional'   => FALSE,
      '#attributes' => ['id' => 'report-result-wrapper'],
    ];
    return $form;
  }

  public function report(array &$form, FormStateInterface $form_state) {
    return [
      '#markup' => 'report:' . $this->title . ' ' . $this->user,
    ];
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->title = $form_state->getValue('title');
    $this->user = 'drupal';
    $this->messenger()->addStatus('title:' . $this->title);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
