<?php
/**
 * 演示表单AJAX操作
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class YunkeForm extends FormBase {

  public function getFormId() {
    return 'yunke_help_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $typeOptions = [
      'cn'     => t('中文汉字'),
      'number' => t('数字'),
      'letter' => t('大小写字母'),
      'mix'    => t('混合'),
    ];
    $form['type'] = [
      '#type'          => 'checkboxes',
      '#title'         => t('验证码字符类型'),
      '#required'      => TRUE,
      '#multiple'      => TRUE,
      '#options'       => $typeOptions,
      '#default_value' => ['cn', 'letter'],
    ];

    $form['#attributes']['target'] = "_blank";
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('submit'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $v = $form_state->getValues()['type'];
    $b = array_filter($v);
    var_dump($v);
    var_dump($b);
    die;
    //some code
    //在这里执行正常提交操作
    //$form_state->set('submitOK', TRUE);
    //标识提交已执行成功
  }

}
