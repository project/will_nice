<?php
/**
 * 演示表单AJAX操作
 */

namespace Drupal\yunke_help\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class YunkeForm extends FormBase
{
    public function getFormId()
    {
        return 'yunke_help_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['value'] = array(
            '#type'        => 'entity_autocomplete',
            //设置自动完成功能
            '#target_type' => 'node',
            //必选参数，传递实体类型
            '#attributes'  => ['data-autocomplete-first-character-blacklist' => '/#?'],
            //设置不自动完成的特殊字符
            '#title'       => '输入值',
            '#description' => "输入字符会自动呈现实体以供选择",
        );
        $form['#attributes']['target'] = "_blank";
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type'        => 'submit',
            '#value'       => $this->t('submit'),
            '#button_type' => 'primary',
        );
        return $form;

    }

    public function validateForm(array & $form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValue('value'))) {
            $form_state->setErrorByName('value', '输入不能为空');
        }
    }

    public function submitForm(array & $form, FormStateInterface $form_state)
    {
        $form_state->cleanValues();
        print_r($form_state->getValues());die;
        //some code
        //在这里执行正常提交操作
        $form_state->set('submitOK', TRUE);
        //标识提交已执行成功
    }
}
