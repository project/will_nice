/**
 * 生成数据图表
 */

(function ($, Drupal) {
  /**
   * Perform document ready tasks through the Drupal API
   * 通过Drupal API 执行文档就绪任务
   */
  Drupal.behaviors.yunkeOrderReportAutoAttach = {
    attach(context, settings) {
      const $context = $(context);
      let reportElementID = '#yunkeOrderReportChart'; //元素id
      let elements;
      elements = $context.find(reportElementID);
      elements.once('yunkeOrderReport').each(function (i) {
        var yunkeChart = new Chart(this, {
          type: 'bar',
          data: {
            labels: yunkeDate,
            datasets: [
              {
                label: '订单数',
                data: yunkeOrder,
                backgroundColor: 'rgba(0, 255, 0, 0.5)', //可以使用这种格式'#CB4335'
                //borderColor: 'rgba(0, 0, 255, 1)',
                //borderWidth: 1,
                yAxisID: 'y',
                borderRadius: 10, //圆角
              },
              {
                label: "实收款",
                data: yunkeMoney,
                backgroundColor: 'rgba(255, 255, 0, 0.5)',
                type: "line",
                tension: 0.4, //拐角系数
                fill: true,
                yAxisID: 'y1',
              }
            ]
          },
          options: {
            responsive: true,
            plugins: {
              title: {
                display: true,
                text: '最近' + yunkeDate.length + '天订单量与实收款',
              },
              tooltip: {
                position: 'nearest',
              },
            },
            scales: {
              x: {
                title: {
                  display: true,
                  text: '订单日期',
                  font: {
                    size: 20,
                    weight: 'bold',
                    lineHeight: 1.2,
                  },
                }
              },
              y: {
                type: 'linear',
                display: true,
                position: 'left',
                title: {
                  display: true,
                  text: '订单数（个）'
                },
              },
              y1: {
                type: 'linear',
                display: true,
                position: 'right',
                title: {
                  display: true,
                  text: '实收款（元）'
                },

                // grid line settings
                grid: {
                  drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
              },
            },
            animation: {
              duration: 2000,
            }
          }
        });


      });
    },
    detach(context, settings, trigger) {
      const $context = $(context);
      let elements;
      if (trigger === 'unload') {
        //nothing to do
      }
    },
  };


}(jQuery, Drupal));
