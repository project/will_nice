<?php

/**
 *  统一平台客户端下单接口
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse; //返回json响应
use Drupal\Core\Url;
use \Drupal\yunke_order\OpenAPI;


/**
 * @package Drupal\yunke_order\Controller
 */
class Order extends ControllerBase {

  //日志记录器
  protected $logger;

  public function __construct() {
    $this->logger = $this->getLogger('yunke_order');
  }


  /**
   * 下单成功后给客户端返回含下单跳转链接的json，返回json响应对象
   */
  public function index() {
    $userDada = (array) json_decode(file_get_contents("php://input"));
    if (!isset($userDada['user_id'])) {
      $response = ['code' => 4001, 'msg' => '参数错误'];
      $this->logger->warning('收到不带用户id的下单请求');
      return new JsonResponse($response);
    }
    $userId = (int) $userDada['user_id'];
    try {
      $openAPI = new OpenAPI($userId);
    } catch (\Exception $e) {
      $response = ['code' => 4001, 'msg' => '参数错误' . $e->getMessage()];
      $this->logger->warning('下单请求参数错误：' . $response['msg']);
      return new JsonResponse($response);
    }

    //验证签名
    if (!$openAPI->verifySign($userDada)) {
      $response = ['code' => 4002, 'msg' => '签名验证错误'];
      $this->logger->error('用户签名验证失败，有人试图冒用用户身份，用户id为：' . $userId);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //请求参数验证
    $verifyResult = $openAPI->verifyParameters($userDada);
    if ($verifyResult !== TRUE) {
      $response = ['code' => 4001, 'msg' => '参数验证错误：' . $verifyResult];
      $this->logger->error('用户下单请求参数验证失败，用户id为：' . $userId . '错误信息为：' . $verifyResult);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //验证通过 开始处理订单
    $order = $userDada;
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    //用户系统必须保证用户订单唯一
    $orderIds = $storager->getQuery('AND')->condition("type", "order", '=')
      ->condition("field_user_order", $order['order_number'], '=')->execute();
    if (empty($orderIds)) {
      //新订单
      $orderEntity = [
        'type'                  => 'order',  //订单内容类型
        'uid'                   => $userId,//系统订单生成用户账号 目前同商家ID
        'title'                 => _yunke_order_getOrderNumber(), //系统订单号
        'field_user_id'         => $userId,//商家ID
        'field_user_order'      => $order['order_number'], //商户订单号
        'field_description'     => $order['description'], //订单描述
        'field_total'           => (int) ($order['total'] * 100), //付款金额 转化为分
        'field_amount'          => 0,//实收款，单位为分，初始状态收到必为0
        'field_timeout_express' => $order['timeout_express'],//交易关闭时间
        'field_notify_url'      => $order['notify_url'], //将付款状态异步通知到商家的链接
        'field_redirect_url'    => $order['return_url'], //付款完成后重定向到商家的链接
        'field_order_state'     => YK_ORDER_STATE_WAIT, //0|等待付款、1|已经付款、2|付款失败、3|等待退款、4|部分退款、5|全部退款
        'field_notify_state'    => YK_ORDER_NOTICE_NONE, //0|尚未处理、1|已收通知、2|已主动查询
        'field_channel'         => 'none', //none|未支付、Alipay|支付宝、Wechat|微信
        'field_attach'          => $order['attach'], //商家自定义附加数据
        'field_cash'            => FALSE,//提现状态
      ];
      $nodeEntity = $storager->create($orderEntity); //创建一个订单（创建一个实体）
      $nodeEntity->save(); //保存订单实体
      $route_parameters = [
        'order' => $nodeEntity->title->value,
      ];
      $options = [
        'absolute' => TRUE,
      ];
      $url = new Url('yunke_order.pay', $route_parameters, $options);
      $response = ['code' => 2000, 'msg' => 'ok', 'url' => $url->toString(FALSE)];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }
    else {
      //订单已经存在
      $nodeEntity = $storager->load(array_shift($orderIds));
      $orderPayState = $nodeEntity->field_order_state->value;
      if ($orderPayState == YK_ORDER_STATE_WAIT || $orderPayState == YK_ORDER_STATE_FALSE) {
        //对于未付款订单和付款失败的订单，更新订单并再次付款
        $nodeEntity->field_description = $order['description'];//订单描述
        $nodeEntity->field_total = (int) ($order['total'] * 100); //付款金额 将元转变为分
        $nodeEntity->field_timeout_express = $order['timeout_express'];//交易关闭时间
        $nodeEntity->field_notify_url = $order['notify_url']; //将付款状态异步通知到商家的链接
        $nodeEntity->field_redirect_url = $order['return_url']; //付款完成后重定向到商家的链接
        $nodeEntity->field_attach = $order['attach']; //商家自定义附加数据
        $nodeEntity->save(); //保存订单实体
        $route_parameters = ['order' => $nodeEntity->title->value,];
        $options = ['absolute' => TRUE,];
        $url = new Url('yunke_order.pay', $route_parameters, $options);
        $response = ['code' => 2000, 'msg' => 'ok', 'url' => $url->toString(FALSE)];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }
      elseif ($orderPayState == YK_ORDER_STATE_SUCCESS) {
        //处理已付款成功的订单
        $this->logger->warning('试图对已付款订单再次付款，已拒绝');
        $response = ['code' => 4003, 'msg' => '订单已付款，请勿重复提交',];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }
      elseif ($orderPayState == YK_ORDER_STATE_REFUND_PROGRESS
        || $orderPayState == YK_ORDER_STATE_REFUND_PART
        || $orderPayState == YK_ORDER_STATE_REFUND_FULL) {
        //处理已退款或退款中的订单
        $this->logger->warning('试图对已退款或退款中的订单再次付款，已拒绝');
        $response = ['code' => 4004, 'msg' => '订单已退款或处于退款中，再次付款须另外下单',];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }
      else {
        $response = ['code' => 4000, 'msg' => '该订单不能付款，请另外下单',];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }

    }

  }

}
