<?php

/**
 *  用于接收退款是否成功的异步通知
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use \GuzzleHttp\Exception\RequestException;
use \Drupal\yunke_order\OpenAPI;
use \Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * 自定义控制器
 * 用于接收退款是否成功的异步通知
 * 即使是在维护模式下，该控制器也可以访问的
 *
 * @package Drupal\yunke_order\Controller
 */
class RefundNotify extends ControllerBase {

  //日志器
  protected $logger;

  public function __construct() {
    $this->logger = $this->getLogger("yunke_order");
  }


  /**
   * 接收到退款状态异步通知后的中转操作动作
   * type Alipay|Wechat
   */
  public function index($type = NULL) {
    $type = $type ? strtolower($type) : 'error';
    if ($type === 'alipay') {
      //支付宝是实时退款  没有异步通知
      return new Response('success');
    }
    elseif ($type === 'wechat') {
      $this->wechat();
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
    }
    else {
      return new Response('404 error', 404);
    }

  }

  public function wechat() {
    //调用服务验证签名
    $request = \Drupal::request();
    $wechatService = \Drupal::service('yunke_pay.pay.wechat');
    if (!$wechatService->verifyNotify($request)) {
      //验签失败
      $this->logger->warning('微信支付异步退款通知签名验证失败！');
      return FALSE;
    }

    //解密数据
    $result = $wechatService->decrypt($request);
    if ($result == FALSE) {
      return FALSE;
    }

    if (!isset($result['out_trade_no'], $result['out_refund_no'])) {
      $this->logger->warning('微信支付异步退款通知中缺乏退款单信息！');
      return FALSE;
    }
    $orderNumber = $result['out_trade_no'];
    $refundNumber = $result['out_refund_no'];
    $refundNumber = substr($refundNumber, strlen($orderNumber));//还原为本系统的订单号

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $refundIds = $storager->getQuery('AND')
      ->condition("type", "refund", '=')
      ->condition("title", $orderNumber, '=')
      ->condition("field_refund_number", $refundNumber, '=')
      ->execute();
    if (empty($refundIds)) {
      $this->logger->warning('微信支付异步退款通知中的退款单不存在！');
      return FALSE;
    }
    $refundEntity = $storager->load(array_shift($refundIds));

    //判断退款是否成功有三种渠道：
    //1、退款申请返回的信息（属于立即处理退款的渠道会即时反馈状态,当同一个退款单再次退款也会即时返回）
    //2、退款异步通知
    //3、退款主动查询
    $refundState = $refundEntity->field_refund_state->value;
    //退款查询可能发生在任意时刻 如果是已经成功退款的订单 不进行联动查询，以本系统的退款状态为准
    //如果是未退款或退款失败的订单，则向渠道联动查询一次
    if ($refundState != YK_ORDER_REFUND_STATE_WAIT && $refundState != YK_ORDER_REFUND_STATE_FALSE) {
      return $this->refundNotify($refundEntity);
    }

    //进行锁定操作 只有得到了锁才能执行操作，否则应该继续等待或放弃执行
    //涉及当前订单的三种操作：退款处理、退款异步通知、退款查询都会操作订单实体
    //如果不进行锁定，会导致对订单实体的操作争抢 出现意外结果
    $operationID = 'refund_' . $refundEntity->title->value;
    //代表着将要进行的操作的标识符，往往可以设置为函数名，就是数据库的name值
    $lock = \Drupal::lock(); //得到锁对象
    if ($lock->acquire($operationID)) {
      $data = print_r($result, TRUE);
      $refundEntity->field_channel_data->value = $data;
      if (!isset($result['refund_status']) || $result['refund_status'] != 'SUCCESS') {
        $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
      }
      else {
        //退款成功
        $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_SUCCESS;
        if (!empty($result['success_time'])) {
          $refundEntity->field_success_time->value = strtotime($result['success_time']);
        }
      }

      //开启数据库事务 确保退款实体和订单实体具备原子性
      $con = \Drupal::database();
      $yunke = $con->startTransaction();
      try {
        $refundEntity->save();
        //退款成功后更新订单实体
        if ($refundEntity->field_refund_state->value == YK_ORDER_REFUND_STATE_SUCCESS) {
          $this->refundAfter($refundEntity);
        }
      } catch (\Exception $e) {
        $msg = "退款异步通知时，退款实体（" . $refundEntity->id() . "）或订单实体";
        $msg .= "保存异常，数据已回滚，异常消息：" . $e->getMessage();
        \Drupal::logger('yunke_order')->error($msg);
        $con->rollback($yunke->name());  //回滚事务到回滚点
      }
      unset($yunke);//变量被销毁，事务被提交
      //不要使用$con->commit();去提交一个事务，这会抛出异常，系统会隐式自动提交
      $lock->release($operationID);
      //脚本结束会自动释放 但当操作结束后应当明确释放锁，依赖锁自动过期有性能损耗
    }
    //通知客户端系统
    $this->refundNotify($refundEntity);
    return TRUE;
  }


  /**
   * 当退款成功时，更新订单实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function refundAfter(NodeInterface $refundEntity) {
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("title", $refundEntity->title->value, '=')
      ->condition("field_user_order", $refundEntity->field_user_order->value, '=')
      ->execute();
    $orderEntity = $storager->load(array_shift($orderIds));
    $orderEntity->field_amount->value -= $refundEntity->field_refund_amount->value; //减小实收款
    $refundingCount = $storager->getQuery('AND')
      ->count()
      ->condition("type", "refund", '=')
      ->condition("title", $orderEntity->title->value, '=')
      ->condition("field_refund_state", [YK_ORDER_REFUND_STATE_WAIT, YK_ORDER_REFUND_STATE_FALSE], 'IN')
      ->execute();
    if ($refundingCount == 0) {
      //没有退款中的订单 则退款必定是部分退款或全部退款
      if ($orderEntity->field_amount->value == 0) { //实收款为0说明已经全部退款
        $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_FULL;
      }
      else {
        $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_PART;
      }
      //是否执行一次数据库查询验证呢？ 成功退款总额 必定等于订单总额  否则发生了系统错误
      //这由数据库事务的原子性进行保障  由于性能原因  不用再验证 长时间（数月）运行后 可执行校验
    }
    $orderEntity->save();
  }

  /**
   * 发送退款异步通知
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return bool|void
   */
  protected function refundNotify(NodeInterface $refundEntity) {
    if (empty($refundEntity->field_notify_url->uri)) {
      return;
    }
    $userId = $refundEntity->field_user_id->target_id;
    try {
      $openAPI = new OpenAPI($userId);
    } catch (\Exception $e) {
      return FALSE;
    }

    $client = \Drupal::httpClient();
    $clientURL = $refundEntity->field_notify_url->uri;
    $response = $this->response($refundEntity);
    $openAPI->addSign($response);
    $HTTPOptions = [
      'json'    => $response,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $json = json_decode($resp->getBody()->getContents()); //响应
      //$json应该等于‘succee’
      return;
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "退款异步通知请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      $this->logger->warning($msg);
      return;
    }
  }

  /**
   * 获取同步以及异步退款响应数据
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return array
   */
  protected function response(NodeInterface $refundEntity) {
    $response = [
      'code'          => 2000,
      'msg'           => 'ok',
      'user_id'       => (int) $refundEntity->field_user_id->target_id, //客户端系统id
      'order_number'  => $refundEntity->field_user_order->value, //商户订单号
      'system_number' => $refundEntity->title->value, //系统订单号
      'refund_number' => $refundEntity->field_refund_number->value, //退款单号
      'amount'        => (float) ($refundEntity->field_refund_amount->value / 100), //退款金额
      'total'         => (float) ($refundEntity->field_total->value / 100), //订单总付款金额
      'refund_state'  => (int) $refundEntity->field_refund_state->value, //退款状态
      'refund_time'   => (int) $refundEntity->field_refund_time->value, //退款发起时间
    ];
    if (!empty($refundEntity->field_refund_reason->value)) {
      $response['reason'] = $refundEntity->field_refund_reason->value;//退款原因
    }
    if (!empty($refundEntity->field_notify_url->uri)) {
      $response['notify_url'] = $refundEntity->field_notify_url->uri;//退款异步通知
    }
    if (!empty($refundEntity->field_success_time->value)) {
      $response['success_time'] = (int) $refundEntity->field_success_time->value;//退款成功时间
    }
    return $response;
  }

}

