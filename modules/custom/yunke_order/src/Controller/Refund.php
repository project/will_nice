<?php

/**
 *  订单退款接口
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\yunke_order\OpenAPI;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\node\NodeInterface;

/**
 * @package Drupal\yunke_order\Controller
 */
class Refund extends ControllerBase {

  //日志器
  protected $logger;

  //用户信息实体 用于判断退款时间范围
  protected $userInfoEntity;

  //订单实体
  protected $orderEntity;

  //退款实体
  protected $refundEntity;

  //用户接口对象
  protected $openAPI;

  public function __construct() {
    $this->logger = $this->getLogger('yunke_order');
  }

  /**
   * 接收来自用户的json查询数据，验签，返回查询结果
   */
  public function index() {
    $userDada = (array) json_decode(file_get_contents("php://input"));

    //验证用户
    if (!isset($userDada['user_id'])) {
      $response = ['code' => 4001, 'msg' => '参数错误'];
      $this->logger->warning('收到不带用户id的退款请求');
      return new JsonResponse($response);
    }
    $userId = (int) $userDada['user_id'];
    try {
      $openAPI = new OpenAPI($userId);
    } catch (\Exception $e) {
      $response = ['code' => 4001, 'msg' => '参数错误' . $e->getMessage()];
      $this->logger->warning('退款请求参数错误：' . $response['msg']);
      return new JsonResponse($response);
    }
    $this->openAPI = $openAPI;
    $this->userInfoEntity = $openAPI->getUserInfoEntity();

    //验证签名
    if (!$openAPI->verifySign($userDada)) {
      $response = ['code' => 4002, 'msg' => '签名验证错误'];
      $this->logger->error('用户签名验证失败，有人试图冒用用户身份，用户id为：' . $userId);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    // 验证退款接口参数
    $verifyResult = $openAPI->verifyParameters($userDada, 'refund');
    if ($verifyResult !== TRUE) {
      $response = ['code' => 4001, 'msg' => '参数验证错误：' . $verifyResult];
      $this->logger->error('客户端系统退款请求参数验证失败，用户id为：' . $userId . '错误信息为：' . $verifyResult);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //验证订单存在性
    $userOrderNumber = $userDada['order_number']; //验证通过说明参数存在且正确
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("field_user_id", $userId, '=')
      ->condition("field_user_order", $userOrderNumber, '=')
      ->execute();
    if (empty($orderIds)) {
      $this->logger->warning('客户端系统退款的订单号不存在，用户id为：' . $userId);
      $response = ['code' => 4005, 'msg' => '订单不存在'];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }
    $this->orderEntity = $orderEntity = $storager->load(array_shift($orderIds));

    //验证订单是否已经付款
    $orderState = $orderEntity->field_order_state->value;
    if ($orderState == YK_ORDER_STATE_WAIT || $orderState == YK_ORDER_STATE_FALSE) {
      $this->logger->warning('客户端系统申请退款的订单尚未付款，用户id为：' . $userId);
      $response = ['code' => 4006, 'msg' => '申请退款的订单尚未付款'];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //检查是否允许退款，如果已经提现则不允许
    if ($orderEntity->field_refund_disable->value != YK_ORDER_REFUND_ALLOW) {
      $this->logger->warning('客户端系统申请退款的订单系统不允许退款，用户id为：' . $userId);
      $response = ['code' => 4007, 'msg' => '申请退款的订单不允许退款'];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //判断退款申请是否为新的
    $systemOrderNumber = $orderEntity->title->value;
    if (isset($userDada['refund_number'])) {
      $refundNumber = $userDada['refund_number'];
    }
    else {
      $refundNumber = $userOrderNumber; //一次性退款采用商户订单号作为退款单号
      if ((int) ($userDada['amount'] * 100) != $orderEntity->field_total->value) {
        //如果没有设置退款单号，说明是一次性全额退款
        $this->logger->warning('refund_number：退款单号缺失，用户id为：' . $userId);
        $response = ['code' => 4008, 'msg' => 'refund_number：退款单号缺失'];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }
    }

    $refundIds = $storager->getQuery('AND')
      ->condition("type", "refund", '=')
      ->condition("title", $systemOrderNumber, '=')
      ->condition("field_refund_number", $refundNumber, '=')
      ->execute();
    if (!empty($refundIds)) {
      $this->refundEntity = $refundEntity = $storager->load(array_shift($refundIds));
    }
    else {
      //新建退款实体 但尚不执行保存
      $refundData = [
        'type'                => 'refund',
        'title'               => $systemOrderNumber,
        'uid'                 => $userId,
        'field_user_id'       => $userId,
        'field_user_order'    => $userOrderNumber,
        'field_refund_number' => $refundNumber,
        'field_total'         => $orderEntity->field_total->value,
        'field_refund_amount' => (int) ($userDada['amount'] * 100),
        'field_refund_reason' => $userDada['reason'] ?: '',
        'field_notify_url'    => $userDada['notify_url'] ?: '',
        'field_channel'       => $orderEntity->field_channel->value,
        'field_refund_time'   => \Drupal::time()->getRequestTime(),
        'field_refund_state'  => YK_ORDER_REFUND_STATE_WAIT,
        'field_success_time'  => NULL,
      ];
      $this->refundEntity = $refundEntity = $storager->create($refundData);
    }

    if ($this->refundEntity->isNew()) {
      //如果是新退款申请则验证退款时间
      $paymentTimeRange = time() - $orderEntity->field_payment_time->value;
      $systemTimeRange = $this->config('yunke_order.settings')->get('order_refund_limit_time');
      if ($paymentTimeRange > $systemTimeRange || $paymentTimeRange > $this->userInfoEntity->field_refund_limit_time->value) {
        $this->logger->warning('订单退款申请已超时，用户id为：' . $userId);
        $response = ['code' => 4008, 'msg' => '订单退款申请已超时'];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }

      //验证退款申请频率
      $refundCount = $storager->getQuery('AND')
        ->count()
        ->condition("type", "refund", '=')
        ->condition("title", $orderEntity->title->value, '=')
        ->execute();
      if ($refundCount >= YK_ORDER_REFUND_LIMIT_COUNT) {
        $this->logger->warning('单笔订单退款申请次数超限，用户id为：' . $userId);
        $response = ['code' => 4010, 'msg' => '单笔订单退款申请次数超限'];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }

      //验证退款金额
      $refundAmount = $storager->getAggregateQuery('AND')
        ->aggregate("field_refund_amount", "SUM")
        ->condition("type", "refund", '=')
        ->condition("title", $orderEntity->title->value, '=')
        ->execute();
      $refundAmount = (int) $refundAmount[0]['field_refund_amount_sum'];
      $refundAmount += (int) $this->refundEntity->field_refund_amount->value;
      if ($refundAmount > (int) $orderEntity->field_total->value) { //如果是浮点数，则不用用符号比较，而应该用bccomp函数
        //退款金额超过订单总金额
        $this->logger->warning('订单退款金额超过付款金额，用户id为：' . $userId);
        $response = ['code' => 4011, 'msg' => '订单退款金额超过付款金额'];
        $openAPI->addSign($response);
        return new JsonResponse($response);
      }
    }

    //验证退款是否已经成功 已成功则不用发起退款
    if ($refundEntity->field_refund_state->value == YK_ORDER_REFUND_STATE_SUCCESS) {
      $response = $this->response($refundEntity);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    //进行锁定操作 只有得到了锁才能执行操作，否则应该继续等待或放弃执行
    //涉及当前订单的三种操作：退款处理、退款异步通知、退款查询都会操作订单实体
    //如果不进行锁定，会导致对订单实体的操作争抢 出现意外结果
    $operationID = 'refund_' . $refundEntity->title->value;
    //代表着将要进行的操作的标识符，往往可以设置为函数名，就是数据库的name值
    $lock = \Drupal::lock(); //得到锁对象
    if ($lock->acquire($operationID)) {
      // 开始执行退款操作
      $refundEntity = $this->refund($refundEntity);

      //开启数据库事务
      $con = \Drupal::database();
      $yunke = $con->startTransaction();
      try {
        $refundEntity->save();
        //更新订单实体
        $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_PROGRESS;
        if ($refundEntity->field_refund_state->value == YK_ORDER_REFUND_STATE_SUCCESS) {
          //这仅发生在退款会被立即处理的支付渠道情况下
          $orderEntity->field_amount->value -= $refundEntity->field_refund_amount->value; //减小实收款
          $refundingCount = $storager->getQuery('AND')
            ->count()
            ->condition("type", "refund", '=')
            ->condition("title", $orderEntity->title->value, '=')
            ->condition("field_refund_state", [YK_ORDER_REFUND_STATE_WAIT, YK_ORDER_REFUND_STATE_FALSE], 'IN')
            ->execute();
          if ($refundingCount == 0) {
            //没有退款中的订单 则退款必定是部分退款或全部退款
            if ($orderEntity->field_amount->value == 0) { //实收款为0说明已经全部退款
              $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_FULL;
            }
            else {
              $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_PART;
            }
          }
        }
        $orderEntity->save();
      } catch (\Exception $e) {
        $msg = "退款操作时，退款实体（" . $refundEntity->id() . "）";
        $msg .= "或订单实体（" . $orderEntity->id() . "）";
        $msg .= "保存异常，数据已回滚，异常消息：" . $e->getMessage();
        \Drupal::logger('yunke_order')->error($msg);
        $con->rollback($yunke->name());  //回滚事务到回滚点
      }
      unset($yunke);//变量被销毁，事务被提交
      //不要使用$con->commit();去提交一个事务，这会抛出异常，系统会隐式自动提交

      //由于微信退款会发起异步通知，而支付宝是实时处理 为统一起见均发送退款异步通知
      $this->refundNotify($refundEntity);

      $lock->release($operationID);
      //脚本结束会自动释放 但当操作结束后应当明确释放锁，依赖锁自动过期有性能损耗
    }

    $response = $this->response($refundEntity);
    $openAPI->addSign($response);
    return new JsonResponse($response);
  }

  /**
   * 发送退款异步通知
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   */
  protected function refundNotify(NodeInterface $refundEntity) {
    if (empty($refundEntity->field_notify_url->uri)) {
      return;
    }
    $client = \Drupal::httpClient();
    $clientURL = $refundEntity->field_notify_url->uri;
    $response = $this->response($refundEntity);
    $this->openAPI->addSign($response);
    $HTTPOptions = [
      'json'    => $response,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $json = json_decode($resp->getBody()->getContents()); //响应
      //$json应该等于‘succee’
      return;
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "退款异步通知请求失败，响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      $this->logger->warning($msg);
      return;
    }
  }

  /**
   * 发起渠道退款处理
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Drupal\node\NodeInterface
   */
  protected function refund(NodeInterface $refundEntity) {
    $channel = $refundEntity->field_channel->value;
    if ($channel == 'Alipay') {
      //向支付宝发起退款并更新实体
      $refundEntity = \Drupal::service('yunke_order.pay.alipay')->refund($refundEntity);
    }
    elseif ($channel == 'Wechat') {
      //向微信发起退款并更新实体
      $refundEntity = \Drupal::service('yunke_order.pay.wechat')->refund($refundEntity);
    }
    return $refundEntity;
  }

  /**
   * 获取同步以及异步退款响应数据
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return array
   */
  protected function response(NodeInterface $refundEntity) {
    $response = [
      'code'          => 2000,
      'msg'           => 'ok',
      'user_id'       => (int) $refundEntity->field_user_id->target_id, //客户端系统id
      'order_number'  => $refundEntity->field_user_order->value, //商户订单号
      'system_number' => $refundEntity->title->value, //系统订单号
      'refund_number' => $refundEntity->field_refund_number->value, //退款单号
      'amount'        => (float) ($refundEntity->field_refund_amount->value / 100), //退款金额
      'total'         => (float) ($refundEntity->field_total->value / 100), //订单总付款金额
      'refund_state'  => (int) $refundEntity->field_refund_state->value, //退款状态
      'refund_time'   => (int) $refundEntity->field_refund_time->value, //退款发起时间
    ];
    if (!empty($refundEntity->field_refund_reason->value)) {
      $response['reason'] = $refundEntity->field_refund_reason->value;//退款原因
    }
    if (!empty($refundEntity->field_notify_url->uri)) {
      $response['notify_url'] = $refundEntity->field_notify_url->uri;//退款异步通知
    }
    if (!empty($refundEntity->field_success_time->value)) {
      $response['success_time'] = (int) $refundEntity->field_success_time->value;//退款成功时间
    }
    return $response;
  }

}
