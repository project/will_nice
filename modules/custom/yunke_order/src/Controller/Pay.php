<?php

/**
 *  显示订单 并选择支付渠道
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @package Drupal\yunke_order\Controller
 */
class Pay extends ControllerBase {

  /**
   * 下单成功后给客户端系统返回含付款跳转链接的json，返回json响应对象
   */
  public function index($order = '') {
    // 目前付款不需要验证身份，这意味着可以将付款链接发送给他人进行代付

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')->condition("type", "order", '=')->condition("title", $order, '=')->execute();
    if (empty($orderIds)) {
      $this->getLogger('yunke_order')->warning("付款链接订单不存在，当本日志大量出现时，说明可能遭遇洪水攻击");
      //考虑进行限流操作吗？暂时不用
      //return new Response("订单不存在", 404);
      $form = \Drupal::formBuilder()->getForm("\Drupal\yunke_order\Form\Pay404");
      return $form;
    }
    $orderEntity = $storager->load(array_shift($orderIds));
    $merchantIds = $storager->getQuery('AND')
      ->condition("type", 'user', '=')
      ->condition("field_user_id", $orderEntity->field_user_id->target_id, '=')
      ->execute();
    $merchantEntity = $storager->load(array_shift($merchantIds));

    $form = \Drupal::formBuilder()->getForm("\Drupal\yunke_order\Form\SelectPayChannelForm", $orderEntity, $merchantEntity);
    return $form;
  }

}
