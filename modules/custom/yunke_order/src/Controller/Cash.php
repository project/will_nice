<?php

/**
 *  通过批处理执行提现操作 启动页面
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @package Drupal\yunke_order\Controller
 */
class Cash extends ControllerBase {

  protected $destination = NULL;

  /**
   * 执行提现
   */
  public function index($id = '') {
    //确定重定向链接
    $this->destination = \Drupal::request()->query->get('destination') ?: NULL;

    if (empty($id)) {
      return $this->response('提现id缺失');
    }
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $cashEntity = $storager->load($id);
    if (empty($cashEntity)) {
      return $this->response("提现申请(id：{$id})不存在");
    }
    if ($cashEntity->field_cash_state->value != YK_ORDER_CASH_STATE_WAIT) {
      return $this->response("提现申请正在处理或已处理完毕");
    }

    //批处理参数
    $arguments = [];
    $arguments['cashEntityID'] = $id;
    $arguments['cashBatchLimit'] = (int) $this->config('yunke_order.settings')->get('cash_batch_limit') ?: 5;
    $arguments['userID'] = $cashEntity->field_user_id->target_id;
    $arguments['timeLimit'] = strtotime($cashEntity->title->value);

    $arguments['total'] = $storager->getQuery('AND')
      ->count()//求总条数
      ->condition("type", "order", '=')
      ->condition("field_user_id", $arguments['userID'], '=')
      ->condition("field_order_state", [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PART,], 'IN')
      //仅含付款成功和部分退款订单，不包含正在退款中的订单（其可能导致金额低于阈值），也不包含完整退款的订单（其金额为0）
      ->condition("field_cash", FALSE, '=')
      //仅包含未提现订单，已提现的订单不能再提
      ->condition("field_payment_time", $arguments['timeLimit'], '<')
      //仅包含成功付款在当天前的订单 因为支付宝和微信都是T+1到账
      ->execute();

    if (empty($arguments['total'])) {
      return $this->response("没有可提现订单");
    }
    $cashEntity->field_cash_state->value = YK_ORDER_CASH_STATE_PROGRESS;
    $cashEntity->save();

    $operations = [];
    $operations[] = ['\Drupal\yunke_order\DoCash::cash', [$arguments]]; //可传递回调参数
    $batch = [
      'title'            => '客户端系统用户（' . $arguments['userID'] . '）提现处理中，如中断可刷新继续提现',
      'operations'       => $operations,
      'finished'         => '\Drupal\yunke_order\DoCash::cashFinished',
      'init_message'     => '提现初始化中',
      'progress_message' => '已完成： @percentage%  共： @total.',
      'error_message'    => '发生了一个错误',
    ];
    batch_set($batch);
    return batch_process($this->destination);
  }

  /**
   * 返回响应，如果有重定向链接就以状态方式显示反馈信息，否则在页面显示
   *
   * @param $msg
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function response($msg) {
    if ($this->destination) {
      $this->messenger()->addStatus($msg);
      return new RedirectResponse($this->destination);
    }
    return ['#markup' => $msg];
  }

}
