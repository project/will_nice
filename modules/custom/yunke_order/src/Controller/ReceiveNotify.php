<?php

/**
 *  用于接收支付是否成功的异步通知
 *  by:yunke
 *  email:phpworld@qq.com
 *  time:20210520
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use \GuzzleHttp\Exception\RequestException;
use \Drupal\yunke_order\OpenAPI;
use \Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use \WechatPay\GuzzleMiddleware\Util\AesUtil;


/**
 * 自定义控制器
 * 用于接收支付是否成功的异步通知
 * 即使是在维护模式下，该控制器也可以访问的
 *
 * @package Drupal\yunke_order\Controller
 */
class ReceiveNotify extends ControllerBase {

  //日志器
  protected $logger;

  public function __construct() {
    $this->logger = $this->getLogger("yunke_order");
  }


  /**
   * 接收到支付状态异步通知后的中转操作动作
   * type Alipay|Wechat
   */
  public function index($type = NULL) {
    $type = $type ? strtolower($type) : 'error';
    if ($type === 'alipay') {
      $this->alipay();
      return new Response('success');
      //支付宝接口无条件返回'success'  否则异步消息会在25小时内重发8次
      //见：https://opendocs.alipay.com/open/270/105902
    }
    elseif ($type === 'wechat') {
      $this->wechat();
      return new JsonResponse(['code' => 'SUCCESS', "message" => "成功"]);
      //按微信建议返回应答  否则异步消息会在25小时内重发15次
      //通知频率为15s/15s/30s/3m/10m/20m/30m/30m/30m/60m/3h/3h/3h/6h/6h - 总计 24h4m
    }
    else {
      return new Response('404 error', 404);
    }

  }

  public function alipay() {
    //调用服务验签
    if (!(\Drupal::service('yunke_pay.pay.alipay')->verifyNotify($_POST))) {
      //验签失败
      $this->logger->warning('支付宝异步付款通知验签失败！');
      return FALSE;
    }

    $out_trade_no = $_POST['out_trade_no']; //系统订单号
    $trade_no = $_POST['trade_no']; //支付宝订单号
    $buyer_id = $_POST['buyer_id']; //买家支付宝用户号。买家支付宝账号对应的支付宝唯一用户号。以 2088 开头的纯 16 位数字。
    $trade_status = $_POST['trade_status']; //交易状态
    $receipt_amount = $_POST['receipt_amount']; //实收金额。商家在交易中实际收到的款项，单位为元，精确到小数点后 2 位。
    $gmt_payment = $_POST['gmt_payment']; //交易付款时间。该笔交易的买家付款时间。格式为yyyy-MM-dd HH:mm:ss。

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("title", $out_trade_no, '=')->execute();
    if (empty($orderIds)) {
      $this->logger->warning('支付宝异步付款通知中的订单不存在！');
      return;
    }
    $orderEntity = $storager->load(array_shift($orderIds));

    $orderState = $orderEntity->field_order_state->value;
    //如果是已经付款过的订单 不进行处理  这可能发生在异步通知前已经进行主动查询
    if ($orderState != YK_ORDER_STATE_WAIT && $orderState != YK_ORDER_STATE_FALSE) {
      $this->notifyClient($orderEntity);
      return;
    }

    $orderEntity->field_channel_data->value = print_r($_POST, TRUE);
    $orderEntity->field_notify_state->value = YK_ORDER_NOTICE_RECEIVED;
    //标记已收到异步通知 0|尚未处理、1|已收通知、2|已主动查询

    if (
      ($trade_status == 'TRADE_SUCCESS' || $trade_status == 'TRADE_FINISHED')
      && (float) ($orderEntity->field_total->value / 100) == (float) $receipt_amount  //支付宝返回单位是元，系统储存单位是分
    ) {
      //付款成功
      $orderEntity->field_channel->value = 'Alipay'; //保存付款渠道
      $orderEntity->field_channel_order->value = $trade_no; //保存渠道订单号
      $orderEntity->field_buyer_id->value = $buyer_id; //保存买家ID
      $orderEntity->field_payment_time->value = strtotime($gmt_payment); //保存付款时间
      $orderEntity->field_order_state->value = YK_ORDER_STATE_SUCCESS;
      //保存付款状态，0|等待付款、1|成功付款、2|付款失败、3|等待退款、4|部分退款、5|全部退款
      $orderEntity->field_amount->value = $orderEntity->field_total->value; //实收款
    }
    else {
      $orderEntity->field_order_state->value = YK_ORDER_STATE_FALSE;
    }
    $orderEntity->save();
    $this->notifyClient($orderEntity);
  }

  public function wechat() {
    //调用服务验证签名
    $request = \Drupal::request();

    $wechatService = \Drupal::service('yunke_pay.pay.wechat');
    if (!$wechatService->verifyNotify($request)) {
      //验签失败
      $this->logger->warning('微信支付异步付款通知签名验证失败！');
      return FALSE;
    }

    //解密数据
    $result = $wechatService->decrypt($request);
    if ($result == FALSE) {
      return FALSE;
    }

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("title", $result['out_trade_no'], '=')->execute();
    if (empty($orderIds)) {
      $this->logger->warning('微信支付异步付款通知中的订单不存在！');
      return FALSE;
    }
    $orderEntity = $storager->load(array_shift($orderIds));

    $orderState = $orderEntity->field_order_state->value;
    //如果是已经付款过的订单 不进行处理  这可能发生在异步通知前已经进行主动查询
    if ($orderState != YK_ORDER_STATE_WAIT && $orderState != YK_ORDER_STATE_FALSE) {
      $this->notifyClient($orderEntity);
      return FALSE;
    }

    $orderEntity->field_channel_data->value = print_r($result, TRUE);
    $orderEntity->field_notify_state->value = YK_ORDER_NOTICE_RECEIVED;
    //标记已收到异步通知 0|尚未处理、1|已收通知、2|已主动查询

    if (isset($result['trade_state']) && $result['trade_state'] == 'SUCCESS') {
      //付款成功
      $orderEntity->field_channel->value = 'Wechat'; //保存付款渠道
      if (isset($result['transaction_id'])) {
        $orderEntity->field_channel_order->value = $result['transaction_id']; //保存渠道订单号
      }
      if (isset($result['payer']->openid)) {
        $orderEntity->field_buyer_id->value = $result['payer']->openid; //保存买家ID
      }
      if (isset($result['success_time'])) {
        $orderEntity->field_payment_time->value = strtotime($result['success_time']);//保存付款时间
      }
      else {
        $orderEntity->field_payment_time->value = time();//保存付款时间 必须保存 这用于提现
      }

      //交易支付成功
      $orderEntity->field_order_state->value = YK_ORDER_STATE_SUCCESS;
      $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_ALLOW;
      $orderEntity->field_amount->value = $orderEntity->field_total->value; //实收款
    }
    else {
      $orderEntity->field_order_state->value = YK_ORDER_STATE_FALSE;
    }
    $orderEntity->save();
    $this->notifyClient($orderEntity);
  }


  /**
   * 处理用户付款异步通知到客户端
   *
   * @param \Drupal\node\NodeInterface $orderEntity
   *
   * @return bool|void
   * @throws \Exception
   */
  protected function notifyClient(NodeInterface $orderEntity) {
    if (empty($orderEntity->field_notify_url->uri)) {
      return; //如果用户没有传递异步通知链接，那么不通知
    }
    $client = \Drupal::httpClient();
    $clientURL = $orderEntity->field_notify_url->uri;
    $userId = (int) $orderEntity->field_user_id->target_id;
    //异步通知的内容
    $order = [
      'user_id'         => $userId, //客户端系统id
      'order_number'    => $orderEntity->field_user_order->value, //商户订单号
      'system_number'   => $orderEntity->title->value, //系统订单号
      'description'     => $orderEntity->field_description->value, //订单描述
      'total'           => $orderEntity->field_total->value / 100, //订单金额 单位分转化为元
      'amount'          => $orderEntity->field_amount->value / 100, //实收金额 单位分转化为元
      'timeout_express' => (int) $orderEntity->field_timeout_express->value, //交易关闭时间
      'order_state'     => (int) $orderEntity->field_order_state->value, //付款状态
      'order_time'      => (int) $orderEntity->field_payment_time->value, //付款时间
      'attach'          => $orderEntity->field_attach->value, //付款时间
    ];
    //签名
    $openAPI = new OpenAPI($userId);
    $openAPI->addSign($order);
    $HTTPOptions = [
      'json'    => $order,
      'headers' => ['Accept' => 'application/json'],
    ];
    try {
      $resp = $client->post($clientURL, $HTTPOptions);
      $jsonResponse = json_decode($resp->getBody()->getContents()); //响应数组
      if ($jsonResponse instanceof \stdClass) {
        $jsonResponse = (array) $jsonResponse;
      }
      //判断客户端是否正确接收异步通知
      if ($jsonResponse != 'success') {
        //$this->logger->warning('付款异步通知客户端系统返回值不正确，客户端id：' . $userId);
      }
      //暂不判断  只要发送即可  避免产生大量日志信息  推荐客户端进行主动查询
      return TRUE;
    } catch (RequestException $e) {
      //HTTP状态码大于等于400 或者网络错误将报错触发这里
      $msg = "付款异步通知用户({$userId})失败，消息：" . $e->getMessage() . "\n";
      if ($e->hasResponse()) {
        $msg .= "用户响应状态码：" . $e->getResponse()->getStatusCode() . "；响应body:" . $e->getResponse()->getBody() . "\n";
      }
      $this->logger->warning($msg);
    } catch (\Exception $e) {
      $msg = "付款异步通知用户({$userId})失败，消息：" . $e->getMessage();
      $this->logger->warning($msg);
    }

  }

}

