<?php

/**
 *  订单退款状态查询接口
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\yunke_order\OpenAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\node\NodeInterface;

/**
 * @package Drupal\yunke_order\Controller
 */
class QueryRefund extends ControllerBase {

  protected $logger;

  protected $openAPI;

  public function __construct() {
    $this->logger = $this->getLogger('yunke_order');
  }

  /**
   * 接收来自用户的json查询数据，验签，返回查询结果
   */
  public function index() {
    $userDada = (array) json_decode(file_get_contents("php://input"));
    if (!isset($userDada['user_id'])) {
      $response = ['code' => 4001, 'msg' => '参数错误'];
      $this->logger->warning('收到不带用户id的退款查询请求');
      return new JsonResponse($response);
    }
    $userId = (int) $userDada['user_id'];
    try {
      $openAPI = new OpenAPI($userId);
    } catch (\Exception $e) {
      $response = ['code' => 4001, 'msg' => '参数错误' . $e->getMessage()];
      $this->logger->warning('退款查询请求参数错误：' . $response['msg'] . " 用户id：" . $userId);
      return new JsonResponse($response);
    }
    $this->openAPI = $openAPI;
    //验证签名
    if (!$openAPI->verifySign($userDada)) {
      $response = ['code' => 4002, 'msg' => '签名验证错误'];
      $this->logger->warning('用户签名验证失败，有人试图冒用用户身份，用户id为：' . $userId);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    // 验证退款查询接口参数
    $verifyResult = $openAPI->verifyParameters($userDada, 'query_refund');
    if ($verifyResult !== TRUE) {
      $response = ['code' => 4001, 'msg' => '参数验证错误：' . $verifyResult];
      $this->logger->warning('客户端系统退款查询请求参数验证失败，用户id为：' . $userId . '错误信息为：' . $verifyResult);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    $orderNumber = $userDada['order_number']; //验证通过说明参数存在且正确
    $refundNumber = $userDada['refund_number'] ? $userDada['refund_number'] : $orderNumber;
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "refund", '=')
      ->condition("field_user_id", $userId, '=')
      ->condition("field_user_order", $orderNumber, '=')
      ->condition("field_refund_number", $refundNumber, '=')
      ->execute();
    if (empty($orderIds)) {
      $this->logger->warning('客户端系统查询的退款单不存在，用户id为：' . $userId);
      $response = ['code' => 4005, 'msg' => '退款单不存在'];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }
    $refundEntity = $storager->load(array_shift($orderIds));
    //判断退款是否成功有三种渠道：
    //1、退款申请返回的信息（属于立即处理退款的渠道会即时反馈状态）
    //2、退款异步通知
    //3、退款主动查询
    $refundState = $refundEntity->field_refund_state->value;
    //退款查询可能发生在任意时刻 如果是已经成功退款的订单 不进行联动查询，以本系统的退款状态为准
    //如果是未退款或退款失败的订单，则向渠道联动查询一次
    if ($refundState != YK_ORDER_REFUND_STATE_WAIT && $refundState != YK_ORDER_REFUND_STATE_FALSE) {
      return $this->response($refundEntity);
    }

    //进行锁定操作 只有得到了锁才能执行操作，否则应该继续等待或放弃执行
    //涉及当前订单的三种操作：退款处理、退款异步通知、退款查询都会操作订单实体
    //如果不进行锁定，会导致对订单实体的操作争抢 出现意外结果
    $operationID = 'refund_' . $refundEntity->title->value;
    //代表着将要进行的操作的标识符，往往可以设置为函数名，就是数据库的name值
    $lock = \Drupal::lock(); //得到锁对象
    if ($lock->acquire($operationID)) {
      $channel = $refundEntity->field_channel->value;
      if ($channel == 'Alipay') {
        //联动向支付宝查询并更新实体
        $refundEntity = \Drupal::service('yunke_order.pay.alipay')->queryRefund($refundEntity);
      }
      elseif ($channel == 'Wechat') {
        //联动向微信查询并更新实体
        $refundEntity = \Drupal::service('yunke_order.pay.wechat')->queryRefund($refundEntity);
      }

      //开启数据库事务 确保退款实体和订单实体具备原子性
      $con = \Drupal::database();
      $yunke = $con->startTransaction();
      try {
        $refundEntity->save();//确保实体被保存
        //退款成功后更新订单实体
        if ($refundEntity->field_refund_state->value == YK_ORDER_REFUND_STATE_SUCCESS) {
          $this->refundAfter($refundEntity);
        }
      } catch (\Exception $e) {
        $msg = "退款查询时，退款实体（" . $refundEntity->id() . "）或订单实体";
        $msg .= "保存异常，数据已回滚，异常消息：" . $e->getMessage();
        \Drupal::logger('yunke_order')->error($msg);
        $con->rollback($yunke->name());  //回滚事务到回滚点
      }
      unset($yunke);//变量被销毁，事务被提交
      //不要使用$con->commit();去提交一个事务，这会抛出异常，系统会隐式自动提交
      $lock->release($operationID);
      //脚本结束会自动释放 但当操作结束后应当明确释放锁，依赖锁自动过期有性能损耗
    }
    return $this->response($refundEntity);
  }

  /**
   * 当查询退款成功时，更新订单实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function refundAfter(NodeInterface $refundEntity) {
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("title", $refundEntity->title->value, '=')
      ->condition("field_user_order", $refundEntity->field_user_order->value, '=')
      ->execute();
    $orderEntity = $storager->load(array_shift($orderIds));
    $orderEntity->field_amount->value -= $refundEntity->field_refund_amount->value; //减小实收款
    $refundingCount = $storager->getQuery('AND')
      ->count()
      ->condition("type", "refund", '=')
      ->condition("title", $orderEntity->title->value, '=')
      ->condition("field_refund_state", [YK_ORDER_REFUND_STATE_WAIT, YK_ORDER_REFUND_STATE_FALSE], 'IN')
      ->execute();
    if ($refundingCount == 0) {
      //没有退款中的订单 则退款必定是部分退款或全部退款
      if ($orderEntity->field_amount->value == 0) { //实收款为0说明已经全部退款
        $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_FULL;
      }
      else {
        $orderEntity->field_order_state->value = YK_ORDER_STATE_REFUND_PART;
      }
      //是否执行一次数据库查询验证呢？ 成功退款总额 必定等于订单总额  否则发生了系统错误
    }
    $orderEntity->save();
  }

  /**
   * 退款查询的响应和退款响应一样
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  protected function response(NodeInterface $refundEntity) {
    $response = [
      'code'          => 2000,
      'msg'           => 'ok',
      'user_id'       => (int) $refundEntity->field_user_id->target_id, //客户端系统id
      'order_number'  => $refundEntity->field_user_order->value, //商户订单号
      'system_number' => $refundEntity->title->value, //系统订单号
      'refund_number' => $refundEntity->field_refund_number->value, //退款单号
      'amount'        => $refundEntity->field_refund_amount->value / 100, //退款金额 单位由分转变为元
      'total'         => $refundEntity->field_total->value / 100, //订单总付款金额 单位由分转变为元
      'refund_state'  => (int) $refundEntity->field_refund_state->value, //退款状态
      'refund_time'   => (int) $refundEntity->field_refund_time->value, //退款发起时间
    ];
    if (!empty($refundEntity->field_refund_reason->value)) {
      $response['reason'] = $refundEntity->field_refund_reason->value;//退款原因
    }
    if (!empty($refundEntity->field_notify_url->uri)) {
      $response['notify_url'] = $refundEntity->field_notify_url->uri;//退款异步通知
    }
    if (!empty($refundEntity->field_success_time->value)) {
      $response['success_time'] = (int) $refundEntity->field_success_time->value;//退款成功时间
    }
    $this->openAPI->addSign($response);
    return new JsonResponse($response);
  }

}
