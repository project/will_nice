<?php

/**
 *  订单状态查询接口
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\yunke_order\OpenAPI;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\node\NodeInterface;

/**
 * @package Drupal\yunke_order\Controller
 */
class Query extends ControllerBase {

  protected $logger;

  protected $openAPI;

  public function __construct() {
    $this->logger = $this->getLogger('yunke_order');
  }

  /**
   * 接收来自用户的json查询数据，验签，返回查询结果
   */
  public function index() {
    $userDada = (array) json_decode(file_get_contents("php://input"));
    if (!isset($userDada['user_id'])) {
      $response = ['code' => 4001, 'msg' => '参数错误'];
      $this->logger->warning('收到不带用户id的查询请求');
      return new JsonResponse($response);
    }
    $userId = (int) $userDada['user_id'];
    try {
      $openAPI = new OpenAPI($userId);
    } catch (\Exception $e) {
      $response = ['code' => 4001, 'msg' => '参数错误' . $e->getMessage()];
      $this->logger->warning('查询请求参数错误：' . $response['msg'] . " 用户id：" . $userId);
      return new JsonResponse($response);
    }
    $this->openAPI = $openAPI;
    //验证签名
    if (!$openAPI->verifySign($userDada)) {
      $response = ['code' => 4002, 'msg' => '签名验证错误'];
      $this->logger->warning('用户签名验证失败，有人试图冒用用户身份，用户id为：' . $userId);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    // 验证查询接口参数
    $verifyResult = $openAPI->verifyParameters($userDada, 'query');
    if ($verifyResult !== TRUE) {
      $response = ['code' => 4001, 'msg' => '参数验证错误：' . $verifyResult];
      $this->logger->warning('客户端系统查询请求参数验证失败，用户id为：' . $userId . '错误信息为：' . $verifyResult);
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }

    $order = $userDada['order_number']; //验证通过说明参数存在且正确
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $orderIds = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("field_user_id", $userId, '=')
      ->condition("field_user_order", $order, '=')
      ->execute();
    if (empty($orderIds)) {
      $this->logger->warning('客户端系统查询的订单号不存在，用户id为：' . $userId);
      $response = ['code' => 4005, 'msg' => '订单不存在'];
      $openAPI->addSign($response);
      return new JsonResponse($response);
    }
    $orderEntity = $storager->load(array_shift($orderIds));

    $orderState = $orderEntity->field_order_state->value;
    //查询可能发生在任意时刻 如果是已经付款过的订单 不进行联动查询，以本系统的订单状态为准
    //如果是未付款或付款失败的订单，则向渠道联动查询一次，这可能发生在付款异步通知前进行了主动查询
    if ($orderState != YK_ORDER_STATE_WAIT && $orderState != YK_ORDER_STATE_FALSE) {
      return $this->response($orderEntity);
    }

    $channel = $orderEntity->field_channel->value;
    if ($channel == 'Alipay') {
      //联动向支付宝查询并更新实体
      $orderEntity = \Drupal::service('yunke_order.pay.alipay')->query($orderEntity);
    }
    elseif ($channel == 'Wechat') {
      //联动向微信查询并更新实体
      $orderEntity = \Drupal::service('yunke_order.pay.wechat')->query($orderEntity);
    }
    $orderEntity->save();//确保实体被保存
    return $this->response($orderEntity);
  }

  protected function response(NodeInterface $orderEntity) {
    $order = [
      'code'            => 2000,
      'msg'             => 'ok',
      'user_id'         => (int) $orderEntity->field_user_id->target_id, //客户端系统id
      'order_number'    => $orderEntity->field_user_order->value, //商户订单号
      'system_number'   => $orderEntity->title->value, //系统订单号
      'description'     => $orderEntity->field_description->value, //订单描述
      'total'           => $orderEntity->field_total->value / 100, //订单金额 单位分转化为元
      'amount'          => $orderEntity->field_amount->value / 100, //实收金额 单位分转化为元
      'timeout_express' => (int) $orderEntity->field_timeout_express->value, //交易关闭时间
      'order_state'     => (int) $orderEntity->field_order_state->value, //付款状态
      'order_time'      => (int) $orderEntity->field_payment_time->value, //付款时间
      'attach'          => $orderEntity->field_attach->value, //付款时间
    ];

    $this->openAPI->addSign($order);
    return new JsonResponse($order);
  }

}
