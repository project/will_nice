<?php
/**
 * 模块设置表单
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;

class SettingsForm extends FormBase {

  public function getFormId() {
    return 'yunke_order_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#config'] = $this->config('yunke_order.settings');
    $notice = '在统一收银平台上线运行前，请合理设置本表单';
    $form['notice'] = [
      '#markup' => $notice,
    ];

    $typeOptions = [
      'Alipay' => '支付宝',
      'Wechat' => '微信',
    ];
    $form['defaultPay'] = [
      '#type'          => 'radios',
      '#title'         => '首选支付方式',
      '#required'      => TRUE,
      '#options'       => $typeOptions,
      '#default_value' => $form['#config']->get('defaultPay'),
      '#description'   => '在用户选择支付渠道前，默认选中的支付方式',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['order_refund_limit_time'] = [
      '#type'          => 'number',
      '#title'         => '退款时间限制',
      '#description'   => '平台最大退款时间限制,可退款订单允许的退款时间范围，单位秒，从付款起超过该范围将不能退款，默认30天，客户端系统设置不允许超过此值，但可以更短。',
      '#required'      => TRUE,
      '#min'           => 0,
      '#max'           => 31536000,
      '#step'          => 1,
      '#default_value' => $form['#config']->get('order_refund_limit_time'),
      '#field_suffix'  => '秒',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['cash_amount'] = [
      '#type'          => 'number',
      '#title'         => '默认提现阈值',
      '#description'   => '注意单位为分，为避免大量小额提现，客户端系统每次提现资金必须高于该设置，各客户端系统可以通过预留信息覆写该值',
      '#required'      => TRUE,
      '#min'           => 0,
      '#max'           => 100000000,
      '#step'          => 1,
      '#default_value' => $form['#config']->get('cash_amount'),
      '#field_suffix'  => '分',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['cash_rate'] = [
      '#type'          => 'number',
      '#title'         => '默认提现费率',
      '#description'   => '平台将按该设置扣取手续费，各客户端系统可以通过预留信息覆写该值',
      '#required'      => TRUE,
      '#min'           => 0,
      '#max'           => 1,
      '#step'          => 0.0001,
      '#default_value' => $form['#config']->get('cash_rate'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['cash_batch_limit'] = [
      '#type'          => 'number',
      '#title'         => '提现批处理限制',
      '#description'   => '在订单提现批处理过程中，每次批处理请求处理的订单数量，太大容易造成内存不足，视服务器性能而定，推荐为5',
      '#required'      => TRUE,
      '#min'           => 0,
      '#max'           => 50,
      '#step'          => 1,
      '#default_value' => $form['#config']->get('cash_batch_limit'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['report_days_limit'] = [
      '#type'          => 'number',
      '#title'         => '数据查看限制',
      '#description'   => '查看数据统计时，允许图表展示最近多少天的数据，由于是实时统计，太大将严重影响性能，推荐为7，仅作用于图表',
      '#required'      => TRUE,
      '#min'           => 1,
      '#max'           => 60,
      '#step'          => 1,
      '#default_value' => $form['#config']->get('report_days_limit'),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Submit'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    //进行配置正确性验证
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('yunke_order.settings');
    $config->set('defaultPay', $form_state->getValue('defaultPay'));
    $config->set('order_refund_limit_time', $form_state->getValue('order_refund_limit_time'));
    $config->set('cash_amount', $form_state->getValue('cash_amount'));
    $config->set('cash_rate', $form_state->getValue('cash_rate'));
    $config->set('cash_batch_limit', $form_state->getValue('cash_batch_limit'));
    $config->set('report_days_limit', $form_state->getValue('report_days_limit'));
    $config->save();
    $this->messenger()->addStatus('设置成功');
  }

}
