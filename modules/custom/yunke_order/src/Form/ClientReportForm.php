<?php
/**
 * 客户端系统查看订单数据
 * 为将来具备各种可能情况 本类并不继承管理员数据概览表单
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

class ClientReportForm extends FormBase {

  //预览谁的数据 NULL为全部
  protected $userID = NULL;

  //节点储存器
  protected $storager = NULL;

  //模块配置
  protected $config = NULL;

  //报表数据的默认开始和结束时间
  protected $timeStart = NULL;

  protected $timeEnd = NULL;

  //便于显示最近数据方法recent使用 别无他用
  protected $formState = NULL;

  public function getFormId() {
    return 'yunke_order_client_report_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $currentUser = \Drupal::currentUser();
    if (!in_array('client', $currentUser->getRoles())) {
      $form['notice'] = ['#markup' => '本表单用于客户端系统数据概览，仅client角色账户能访问',]; //匿名用户和维护账户均不能访问
      return $form;
    }
    $this->userID = (int) $currentUser->id();
    $this->storager = $storager = \Drupal::entityTypeManager()->getStorage("node");
    $this->config = $this->config('yunke_order.settings');
    $reportTimeRange = (int) $this->config->get('report_days_limit');
    $currentTime = time();
    $this->timeStart = strtotime(date('Y-m-d', $currentTime - $reportTimeRange * 86400));
    $this->timeEnd = strtotime(date('Y-m-d', $currentTime));
    $this->formState = $form_state;

    $form['#title'] = '订单数据概览';
    $form['recent'] = $this->recent($reportTimeRange);

    $form['time_start'] = [
      '#type'          => 'datetime',
      '#title'         => '起始时间',
      //'#description'   => '所查看数据的起始时间',
      '#required'      => TRUE,
      '#default_value' => DrupalDateTime::createFromTimestamp($this->timeStart),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
      '#prefix'        => Markup::create('<div class="yunkeOrder_time_start yunkeOrderReport">'),
      '#suffix'        => Markup::create('</div>'),
    ];

    $form['time_end'] = [
      '#type'          => 'datetime',
      '#title'         => '截止时间',
      //'#description'   => '所查看数据的截止时间',
      '#required'      => TRUE,
      '#default_value' => DrupalDateTime::createFromTimestamp($this->timeEnd),
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
      '#prefix'        => Markup::create('<div class="yunkeOrder_time_end yunkeOrderReport">'),
      '#suffix'        => Markup::create('</div>'),
    ];

    $form['view'] = [
      '#type'                    => 'button',
      '#value'                   => '查询',
      '#limit_validation_errors' => [],
      '#ajax'                    => [
        'callback' => '::report',
        'wrapper'  => 'report-result-wrapper',
        'prevent'  => 'click',
        'method'   => 'html',
        'progress' => [
          'type'    => 'throbber',
          'message' => '正在生成结果...',
        ],
      ],
      '#prefix'                  => Markup::create('<span class="yunkeOrder_view yunkeOrderReport">'),
      '#suffix'                  => Markup::create('</span>'),
    ];

    $form['content'] = [
      '#type'       => 'container',
      '#optional'   => FALSE,
      '#attributes' => ['id' => 'report-result-wrapper'],
    ];
    $form['content']['report'] = $this->report($form, $form_state);
    $form['#attached']['library'][] = 'yunke_order/report';
    return $form;

  }

  /**
   * 执行数据概览总结查询
   * AJAX提交表单时本方法会被执行三次
   * 第一次：取回表单
   * 第二次：重建表单
   * 第三次：ajax回调执行
   * 默认在重建表单时执行查询
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function report(array &$form, FormStateInterface $form_state) {
    static $isRebuild = FALSE; //表单是否重建标识

    $input = $this->formState->getUserInput();
    if (!empty($input) && (isset($input['form_id']) && ($input['form_id'] == $this->getFormId()))) {
      //正在AJAX提交表单
      if (empty($form_state->get('userTimeStart'))) { //该变量在验证处理器中产生
        return []; // AJAX提交表单时，第一次取回表单阶段不用执行查询 这是无意义的
      }
    }
    if ($isRebuild) {
      return $form['content']['report']; //说明在表单重建阶段已经执行过查询 此时处于ajax回调阶段 直接返回即可
    }
    $timeStart = $this->timeStart;
    $timeEnd = $this->timeEnd;
    $userTimeStart = $form_state->get('userTimeStart');
    if (!empty($userTimeStart)) {
      $timeStart = $userTimeStart;
    }
    $userTimeEnd = $form_state->get('userTimeEnd');
    if (!empty($userTimeEnd)) {
      $timeEnd = $userTimeEnd;
    }
    if (!empty($form_state->get('userTimeStart'))) {
      $isRebuild = TRUE; //避免ajax回调阶段再次执行查询
    }
    return $this->doQuery(min($timeStart, $timeEnd), max($timeStart, $timeEnd), $this->userID);
  }

  /**
   * 执行统计查询
   *
   * @param      $startTime int 开始时间戳
   * @param      $endTime   int 截止时间戳
   * @param null $userID    int 用户id 为空将会查询所有用户
   *
   * @return array 渲染数组
   */
  protected function doQuery($startTime, $endTime, $userID = NULL) {

    //订单总数
    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $allOrder = $query->execute();

    //已付订单数
    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_order_state", [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PROGRESS, YK_ORDER_STATE_REFUND_PART, YK_ORDER_STATE_REFUND_FULL], 'IN');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $paidOrder = $query->execute();

    //未付订单数
    /*
    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_order_state", [YK_ORDER_STATE_WAIT, YK_ORDER_STATE_FALSE, ], 'IN');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    */
    $unpaidOrder = $allOrder - $paidOrder;

    //部分退款订单数
    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_order_state", YK_ORDER_STATE_REFUND_PART, '=');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $partRefundOrder = $query->execute();

    //完全退款订单数
    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_order_state", YK_ORDER_STATE_REFUND_FULL, '=');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $fullRefundOrder = $query->execute();

    //订单总金额
    $query = $this->storager->getAggregateQuery('AND')
      ->aggregate("field_total", "SUM")
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $amount = $query->execute();
    $allAmount = (int) $amount[0]['field_total_sum'];
    $allAmount = $allAmount / 100;//单位转化从分变为元

    //实收总金额
    $query = $this->storager->getAggregateQuery('AND')
      ->aggregate("field_amount", "SUM")
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $amount = $query->execute();
    $gotAmount = (int) $amount[0]['field_amount_sum'];
    $gotAmount = $gotAmount / 100;//单位转化从分变为元

    //支付宝实收总金额
    $query = $this->storager->getAggregateQuery('AND')
      ->aggregate("field_amount", "SUM")
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_channel", 'Alipay', '=');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $amount = $query->execute();
    $gotAlipayAmount = (int) $amount[0]['field_amount_sum'];
    $gotAlipayAmount = $gotAlipayAmount / 100;//单位转化从分变为元

    //微信实收总金额
    /*
    $query = $this->storager->getAggregateQuery('AND')
      ->aggregate("field_amount", "SUM")
      ->condition("type", "order", '=')
      ->condition("created", $startTime, '>=')
      ->condition("created", $endTime, '<')
      ->condition("field_channel", 'Wechat', '=');
    if (!empty($userID)) {
      $query->condition("field_user_id", $userID, '=');
    }
    $amount = $query->execute();
    $gotWechatAmount = (int) $amount[0]['field_amount_sum'];
    $gotWechatAmount = $gotWechatAmount / 100;//单位转化从分变为元
    */
    $gotWechatAmount = $gotAmount - $gotAlipayAmount;


    $userID = $userID ?: '全部';
    $startTime = date('Y-m-d H:i:s', $startTime);
    $endTime = date('Y-m-d H:i:s', $endTime);
    $report = <<<yunke

<table align="left" border="1" cellpadding="1" cellspacing="1" style="width: 850px;">
	<caption>查询数据如下：</caption>
	<thead>
		<tr>
			<th colspan="3" scope="col" style="text-align: center;">客户端系统ID：{$userID} &nbsp;&nbsp;查询起始时间：{$startTime} &nbsp;&nbsp;查询截止时间：{$endTime}</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>订单总数：{$allOrder}</td>
			<td>已付订单数：{$paidOrder}</td>
			<td>未付订单数：{$unpaidOrder}</td>
		</tr>
		<tr>
			<td>部分退款订单数：{$partRefundOrder}</td>
			<td>完全退款订单数：{$fullRefundOrder}</td>
			<td>订单总金额：{$allAmount}元</td>
		</tr>
		<tr>
			<td>实收总金额：{$gotAmount}元</td>
			<td>支付宝实收额：{$gotAlipayAmount}元</td>
			<td>微信实收额：{$gotWechatAmount}元</td>
		</tr>
	</tbody>
</table>

yunke;

    return [
      '#markup' => Markup::create($report),
    ];
  }

  /**
   * 将用户输入值储存到表单状态对象中
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $userTimeStart = $form_state->getValue($form['time_start']['#parents'])->getTimestamp();
    $form_state->set('userTimeStart', $userTimeStart);
    $userTimeEnd = $form_state->getValue($form['time_end']['#parents'])->getTimestamp();
    $form_state->set('userTimeEnd', $userTimeEnd);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //不需要任何操作
  }

  /**
   * 得到当日订单量
   *
   * @param $timeLimit  string 如2021-06-24
   *
   * @return mixed
   */
  protected function getOrderCount($timeLimit) {
    $timeStart = strtotime($timeLimit);
    $timeEnd = $timeStart + 86400;

    $query = $this->storager->getQuery('AND')
      ->count()
      ->condition("type", "order", '=')
      ->condition("created", $timeStart, '>=')
      ->condition("created", $timeEnd, '<');
    if (!empty($this->userID)) {
      $query->condition("field_user_id", $this->userID, '=');
    }
    return $query->execute();
  }

  /**
   * 返回当天实收款总数
   *
   * @param $timeLimit string 如2021-06-24
   *
   * @return float|int
   */
  protected function getOrderSum($timeLimit) {
    $timeStart = strtotime($timeLimit);
    $timeEnd = $timeStart + 86400;

    $query = $this->storager->getAggregateQuery('AND')
      ->aggregate("field_amount", "SUM")
      ->condition("type", "order", '=')
      ->condition("created", $timeStart, '>=')
      ->condition("created", $timeEnd, '<');
    if (!empty($this->userID)) {
      $query->condition("field_user_id", $this->userID, '=');
    }
    $amount = $query->execute();
    $amount = (int) $amount[0]['field_amount_sum'];
    $amount = $amount / 100;//单位转化从分变为元
    return $amount;
  }

  /**
   * 显示最近$days天内的订单数量和实收款走势
   *
   * @param int $days
   *
   * @return array
   */
  protected function recent($days = 7) {
    $input = $this->formState->getUserInput();
    if (!empty($input) && (isset($input['form_id']) && ($input['form_id'] == $this->getFormId()))) {
      return []; //正在提交表单 避免AJAX时出现大量查询操作
    }

    //解析日期
    $days = (int) $days;
    $orderDate = [];
    $currentTime = time();
    for ($i = 1; $i <= $days; $i++) {
      $orderDate[] = date('Y-m-d', $currentTime - $i * 86400);
    }
    $orderDate = array_reverse($orderDate);
    $orderCount = [];
    $orderSum = [];
    //解析传递给js的变量
    $orderDateJS = '[';
    foreach ($orderDate as $date) {
      $orderDateJS .= '"' . $date . '", ';
      $orderCount[] = $this->getOrderCount($date);
      $orderSum[] = $this->getOrderSum($date);
    }
    $orderDateJS .= ']';
    $orderCountJS = '[' . implode(',  ', $orderCount) . ']';
    $orderSumJS = '[' . implode(',  ', $orderSum) . ']';


    //为前端准备图表元素
    //此处正规的做法应该是通过渲染数组输出元素，通过$element['#attached']['drupalSettings']输出js值
    //但为了演示一种对开发者来说快捷简单的方法 这里不这样做  希望更多人喜欢Drupal  她是足够灵活的
    //希望越来越多的中国开发者参与Drupal 做出贡献 云客在此敬候大家 未来很美 一起向前 伙伴来吧
    $markup = <<<yunkeorder

<div class="chart-container" style="position: relative; height:400px; width:850px">
    <canvas id="yunkeOrderReportChart"></canvas>
</div>
<script>
    //var yunkeDate = ["2021.6.17", "2021.6.18", "2021.6.19", "2021.6.20", "2021.6.21", "2021.6.22", "2021.6.23",];
    var yunkeDate = {$orderDateJS};
    //var yunkeOrder = [12, 19, 3, 5, 2, 3, 12];
    var yunkeOrder = {$orderCountJS};
    //var yunkeMoney = [505, 250, 150, 300, 50, 30, 251];
    var yunkeMoney = {$orderSumJS};
</script>

yunkeorder;

    $recent = [
      '#markup' => Markup::create($markup),
    ];

    return $recent;
  }

}
