<?php
/**
 * 客户端系统提现申请表单
 * 该表单只能由用户亲自访问\Drupal\Core\Form\ConfirmFormBase
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Render\Markup;

class ClientCashRequestForm extends FormBase {

  public function getFormId() {
    return 'yunke_order_client_cash_request_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    //该表单只能由用户亲自访问
    $currentUser = \Drupal::currentUser();
    if (!in_array('client', $currentUser->getRoles())) {
      $form['notice'] = ['#markup' => '本表单用于客户端系统提现申请，仅client角色账户能访问',]; //匿名用户和维护账户均不能访问
      return $form;
    }
    $userID = (int) $currentUser->id();
    $form['#userID'] = $userID;
    $config = $this->config('yunke_order.settings');
    $cashAmount = $config->get('cash_amount');//提现阈值
    $cashRate = $config->get('cash_rate');//提现费率

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $form['#storager'] = $storager;

    $userIds = $storager->getQuery('AND')
      ->condition("type", "user", '=')
      ->condition("field_user_id", $userID, '=')
      ->execute();
    if (!empty($userIds)) {
      $userEntity = $storager->load(array_shift($userIds));
      if (!empty($userEntity->field_cash_amount->value)) {
        $cashAmount = $userEntity->field_cash_amount->value; //用用户特定阈值覆写系统默认阈值
      }
      if (!empty($userEntity->field_cash_rate->value)) {
        $cashRate = $userEntity->field_cash_rate->value; //用用户特定费率覆写系统默认费率
      }
    }
    $form['#cashRate'] = $cashRate;

    $notice = "<strong>提现注意事项：</strong><br>";
    $notice .= "1、一天仅可提现一次，在任意时间可提交提现申请<br>";
    $notice .= "2、每次提现的金额不能小于阈值，你的提现阈值为：" . ($cashAmount / 100) . "元<br>";
    $notice .= "3、你收到的实际款项为：提现的订单实收总额度减去平台运营成本，目前该成本费率为：{$cashRate}<br><br>";


    $form['notice'] = [
      '#markup' => Markup::create($notice),
    ];

    //判断是否已经申请过
    $cashNumber = date('Y-m-d');
    $form['#cashNumber'] = $cashNumber;
    $cashIds = $storager->getQuery('AND')
      ->condition("type", "cash", '=')
      ->condition("field_user_id", $userID, '=')
      ->condition("title", $cashNumber, '=') //一天仅可提现一次
      ->execute();
    if (!empty($cashIds)) {
      $form['cash'] = [
        '#markup' => '你今天已申请过提现，请到提现记录中查看',
        '#prefix' => Markup::create('<strong>'),
        '#suffix' => Markup::create('</strong>'),
      ];
      return $form;
    }

    //判断是否已经达到提现阈值金额
    $amount = $storager->getAggregateQuery('AND')
      ->aggregate("field_amount", "SUM") //实收款求和
      ->condition("type", "order", '=')
      ->condition("field_user_id", $userID, '=')
      ->condition("field_order_state", [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PART,], 'IN')
      //仅含付款成功和部分退款订单，不包含正在退款中的订单（其可能导致金额低于阈值），也不包含完整退款的订单（其金额为0）
      ->condition("field_cash", FALSE, '=')
      //仅包含未提现订单，已提现的订单不能再提
      ->condition("field_payment_time", strtotime($cashNumber), '<')
      //仅包含成功付款在当天前的订单 因为支付宝和微信都是T+1到账
      ->execute();
    $amount = (int) $amount[0]['field_amount_sum']; //可提现金额
    $allowCash = $amount >= $cashAmount; //是否可提现

    $form['cash'] = [
      '#markup' => '你可提现金额为：' . ($amount / 100) . '元',
      '#prefix' => Markup::create('<strong>'),
      '#suffix' => Markup::create('</strong>'),
    ];

    if (!$allowCash) {
      $form['cash']['#markup'] .= "， 尚未达到最低可提现金额，本次不能提现";
      return $form;
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => '申请提现',
      '#button_type' => 'primary',
    ];
    return $form;
  }


  public function validateForm(array &$form, FormStateInterface $form_state) {
    //当用户在浏览器中开多个页面去申请时，本验证是必要的
    $cashIds = $form['#storager']->getQuery('AND')
      ->condition("type", "cash", '=')
      ->condition("field_user_id", $form['#userID'], '=')
      ->condition("title", $form['#cashNumber'], '=')
      ->execute();
    if (!empty($cashIds)) {
      $form_state->setError($form, '提现申请已提交，请勿重复提交');
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cashEntity = [
      'type'                 => 'cash',  //提现申请内容类型
      'uid'                  => $form['#userID'],//生成提现申请的用户账号 目前同商家ID
      'field_user_id'        => $form['#userID'], //客户端名称
      'title'                => $form['#cashNumber'],//提现编号
      'field_cash_pay_state' => YK_ORDER_CASH_STATE_PAY_WAIT,
      'field_cash_amount'    => 0,
      'field_amount'         => 0,
      'field_cash_rate'      => $form['#cashRate'],
      'field_cash_state'     => YK_ORDER_CASH_STATE_WAIT,
      'field_cash_msg'       => '等待处理中',
      'status'               => FALSE, //发布状态
    ];
    $nodeEntity = $form['#storager']->create($cashEntity); //创建一个提现实体
    $nodeEntity->save(); //保存实体
    $this->messenger()->addStatus('提现申请已成功，请等待系统处理');
  }

}
