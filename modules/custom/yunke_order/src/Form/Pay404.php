<?php
/**
 * 订单不存在的展示页
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class Pay404 extends FormBase {

  public function getFormId() {
    return 'yunke_order_pay_404_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    //$form['#title'] = $this->config('system.site')->get('name');
    $form['#title']='订单不存在';
    $notice='请返回'; //考虑在将来加一些推广信息
    $form['notice'] = [
      '#markup' => $notice,
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
