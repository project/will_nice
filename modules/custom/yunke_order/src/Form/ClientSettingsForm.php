<?php
/**
 * 客户端系统用户登录后设置客户端信息表单
 * 该表单只能由用户亲自访问
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\Core\Render\Markup;

class ClientSettingsForm extends FormBase {

  public function getFormId() {
    return 'yunke_order_client_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    //该表单只能由用户亲自访问
    $currentUser = \Drupal::currentUser();
    if (!in_array('client', $currentUser->getRoles())) {
      $form['notice'] = ['#markup' => '本表单用于客户端系统预留信息，仅client角色账户能访问',]; //匿名用户和维护账户均不能访问
      return $form;
    }
    $userID = (int) $currentUser->id();

    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $userIds = $storager->getQuery('AND')
      ->condition("type", "user", '=')
      ->condition("field_user_id", $userID, '=')
      ->execute();
    if (empty($userIds)) {
      //新注册客户端
      $userEntity = [
        'type'                    => 'user',  //订单内容类型
        'uid'                     => $userID,//生成用户信息的用户账号 目前同商家ID
        'title'                   => '客户系统id:' . $userID, //客户端名称
        'field_user_id'           => $userID,//商家ID
        'field_user_key'          => $this->getRandStr(32), //通讯密钥
        'field_refund_limit_time' => $this->config('yunke_order.settings')->get('order_refund_limit_time'), //退款时间限制,默认一个月
        'field_cash_amount'       => $this->config('yunke_order.settings')->get('cash_amount'),//提现阈值
        'field_cash_rate'         => $this->config('yunke_order.settings')->get('cash_rate'),//提现费率
        'field_user_notes'        => '',
        'status'                  => FALSE, //发布状态
      ];
      $nodeEntity = $storager->create($userEntity); //创建一个客户端用户信息实体
      $nodeEntity->save(); //保存实体
    }
    else {
      //订单已经存在
      $nodeEntity = $storager->load(array_shift($userIds));
    }
    $form['#entity'] = $nodeEntity;

    $form['id'] = [
      '#markup' => '你的客户端id是：' . $nodeEntity->field_user_id->target_id,
      '#prefix' => Markup::create('<strong>'),
      '#suffix' => Markup::create('</strong>'),
    ];
    /**
     * //该信息在提现页面输出
     * $form['cash_rate'] = [
     * '#markup' => '提现费率：' . $nodeEntity->field_cash_rate->value.'， 提现时将按该费率扣费',
     * '#prefix' => Markup::create('<br /><span>'),
     * '#suffix' => Markup::create('</span>'),
     * ];
     * $form['cash_amount'] = [
     * '#markup' => '提现阈值：' . $nodeEntity->field_cash_amount->value.'分，单次提现不能低于此值，解除接入时将可全部提现',
     * '#prefix' => Markup::create('<br /><span>'),
     * '#suffix' => Markup::create('</span>'),
     * ];
     */

    $form['name'] = [
      '#type'          => 'textfield',
      '#title'         => '名称',
      '#description'   => '将作为收款方名称被显示在付款页面',
      '#required'      => TRUE,
      '#default_value' => $nodeEntity->title->value,
      '#maxlength'     => 80,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['userKey'] = [
      '#type'          => 'textfield',
      '#title'         => '通讯密钥',
      '#description'   => '客户端系统和平台约定的通讯密钥，也需要保存到客户端系统，只有一致才能通讯成功，仅能包含大小写字母、数字和@#$%&-_，必须为32位长',
      '#pattern'       => '^[0-9a-zA-Z@#$%&\-_]{32}$',
      '#required'      => TRUE,
      '#default_value' => $nodeEntity->field_user_key->value,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
      '#prefix'        => Markup::create('<div id="yunke-userKey">'),
      '#suffix'        => Markup::create('</div>'),
    ];

    //处理自动产生密钥
    $form['generateUserKey'] = [
      '#type'                    => 'button',
      '#value'                   => '自动生成密钥',
      '#limit_validation_errors' => [],
      '#attributes'              => [
        'style' => 'display:inline;',
      ],
      '#ajax'                    => [
        'callback' => '::getUserKey',
        'wrapper'  => 'yunke-userKey',
        'prevent'  => 'click',
        'progress' => [
          'type'    => 'throbber',
          'message' => '正在生成...',
        ],
      ],
    ];

    $form['refundLimitTime'] = [
      '#type'          => 'number',
      '#title'         => '退款限制',
      '#description'   => '允许用户退款的最大时间间隔，超过该时间范围将无法退款，如为0则不允许退款，不能大于平台的限制设置 ',
      '#required'      => TRUE,
      '#min'           => 0,
      '#max'           => 31536000,
      '#step'          => 1,
      '#default_value' => $nodeEntity->field_refund_limit_time->value,
      '#field_suffix'  => '秒',
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['userNotes'] = [
      '#type'          => 'textarea',
      '#title'         => '账户备注',
      '#description'   => '请输入提现时的收款账户等备注信息，收款账户可为银行账户(含开户行/账户名/账号/支行信息）、微信、支付宝等，但此处仅为备注，真实转账时将以签约账号为准；有其他备注信息请一并留言',
      '#default_value' => $nodeEntity->field_user_notes->value,
      '#attributes'    => [
        'autocomplete' => 'off',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Submit'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  /**
   * 自动产生密钥
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function getUserKey(array &$form, FormStateInterface $form_state) {
    $form['userKey']['#value'] = $this->getRandStr(32);
    return $form['userKey'];
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    //进行配置正确性验证
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $nodeEntity = $form['#entity'];
    $nodeEntity->title->value = trim($form_state->getValue('name'));
    $nodeEntity->field_user_key->value = trim($form_state->getValue('userKey'));
    $nodeEntity->field_refund_limit_time->value = (int) $form_state->getValue('refundLimitTime');
    $nodeEntity->field_user_notes->value = trim($form_state->getValue('userNotes')); //系统会自动消毒
    $nodeEntity->save();
    $this->messenger()->addStatus('设置成功');
  }

  /**
   * 得到接口加签随机字符串
   *
   * @param $len INT
   *
   * @return string
   */
  private function getRandStr($len) {
    //随机字符串
    $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890@#$%&-_';
    $nonce = '';
    $max = strlen($str) - 1;
    for ($i = 0; $i < $len; $i++) {
      $nonce .= $str[mt_rand(0, $max)];
    }
    return $nonce;
  }

}
