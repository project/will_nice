<?php
/**
 * 选择下单的支付渠道
 */

namespace Drupal\yunke_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;

class SelectPayChannelForm extends FormBase {

  public function getFormId() {
    return 'yunke_order_Select_Pay_Channel_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#orderEntity'] = $orderEntity = $form_state->getBuildInfo()['args'][0];
    $form['#merchantEntity'] = $merchantEntity = $form_state->getBuildInfo()['args'][1];

    $total = $orderEntity->field_total->value / 100;
    //付款总金额
    $form['#title'] = '金额：' . $total . '元';
    $form['merchant'] = ['#markup' => '收款方：' . $merchantEntity->title->value,];
    //$form['order_number'] = ['#markup' => '<br>系统订单号：' . $orderEntity->title->value,];

    $form['order_info'] = [
      '#type'  => 'details',
      '#title' => "订单详情",
      '#open'  => TRUE,
    ];
    $viewMode = 'select_pay_channel'; //管理员可以在后台通过视图模式去管理订单显示
    $form['order_info']['field_description'] = $orderEntity->field_description->view($viewMode);
    //订单描述
    $form['order_info']['field_order_state'] = $orderEntity->field_order_state->view($viewMode);
    //订单状态
    $form['order_info']['field_timeout_express'] = $orderEntity->field_timeout_express->view($viewMode);
    //交易关闭时间
    $form['order_info']['field_user_order'] = $orderEntity->field_user_order->view($viewMode);
    //商户订单号


    //$form['#attributes']['target'] = "_blank";
    $form['actions']['#type'] = 'actions';
    $orderState = $orderEntity->field_order_state->value;
    if ($orderEntity->field_timeout_express->value <= time()) {
      $form['actions']['submit'] = [
        '#type'        => 'submit',
        '#value'       => '订单已超时关闭',
        '#disabled'    => TRUE,
        '#button_type' => 'primary',
      ];
    }
    elseif ($orderState == YK_ORDER_STATE_WAIT || $orderState == YK_ORDER_STATE_FALSE) {

      $typeOptions = [
        'Alipay' => '支付宝',
        'Wechat' => '微信',
      ];
      $form['channel'] = [
        '#type'          => 'radios',
        '#title'         => '选择支付方式',
        '#required'      => TRUE,
        '#options'       => $typeOptions,
        '#default_value' => $this->config('yunke_order.settings')->get('defaultPay'),
        '#attributes'    => [
          'autocomplete' => 'off',
        ],
      ];

      $form['actions']['submit'] = [
        '#type'        => 'submit',
        '#value'       => '付款',
        '#button_type' => 'primary',
      ];
      $form['actions']['refresh'] = [
        '#type'        => 'submit',
        '#value'       => '刷新',
        '#description' => '系统主动查询付款状态后刷新',
        '#validate'    => ['::doRefresh'],
        '#submit'      => ['::refresh'],
      ];
    }
    elseif ($orderState == YK_ORDER_STATE_SUCCESS) {
      $form['actions']['completed'] = [
        '#type'        => 'submit',
        '#value'       => '已付款,点击继续...',
        '#description' => '如您已经完成付款，可以点此验证并继续',
        '#validate'    => ['::validateCompleted'],
        '#submit'      => ['::completed'],
      ];
    }
    elseif ($orderState == YK_ORDER_STATE_REFUND_PROGRESS) {
      $form['actions']['submit'] = [
        '#type'        => 'submit',
        '#value'       => '订单退款中',
        '#disabled'    => TRUE,
        '#button_type' => 'primary',
      ];
    }
    elseif ($orderState == YK_ORDER_STATE_REFUND_PART || $orderState == YK_ORDER_STATE_REFUND_FULL) {
      $form['actions']['submit'] = [
        '#type'        => 'submit',
        '#value'       => '订单已退款',
        '#disabled'    => TRUE,
        '#button_type' => 'primary',
      ];
    }

    $form['actions']['return'] = [
      '#type'        => 'submit',
      '#value'       => '返回',
      '#description' => '在任何情况下均可通过该按钮返回原客户端系统',
      '#submit'      => ['::completed'],
    ];
    return $form;
  }

  /**
   * 主动查询并刷新订单状态
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function doRefresh(array &$form, FormStateInterface $form_state) {
    $channel = $form['#orderEntity']->field_channel->value;
    if (empty($channel)) {
      return; //没有渠道信息，说明尚未付款
    }
    if ($channel == 'Alipay') {
      //查询支付宝订单 并更新订单实体
      \Drupal::service('yunke_order.pay.alipay')->query($form['#orderEntity']);
    }
    elseif ($channel == 'Wechat') {
      //查询微信订单 并更新订单实体
      \Drupal::service('yunke_order.pay.wechat')->query($form['#orderEntity']);
    }

  }

  /**
   * 刷新付款状态后再次显示
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function refresh(array &$form, FormStateInterface $form_state) {
    //刷新后再次显示
    $route_parameters = [
      'order' => $form['#orderEntity']->title->value,
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $url = new Url('yunke_order.pay', $route_parameters, $options);
    $response = new TrustedRedirectResponse($url->toString(FALSE));
    $form_state->setResponse($response);
  }

  /**
   * 未设置验证器的提交按钮将默认执行该验证器
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * 验证付款是否成功
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateCompleted(array &$form, FormStateInterface $form_state) {
    $orderEntity = $form['#orderEntity'];
    if ($orderEntity->field_order_state->value == YK_ORDER_STATE_WAIT
      || $orderEntity->field_order_state->value == YK_ORDER_STATE_FALSE) {
      $form_state->setError($form, '付款尚未成功');
    }
  }

  /**
   * 付款成功，返回客户端系统
   *
   * @param array                                $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function completed(array &$form, FormStateInterface $form_state) {
    $redirect_url = $form['#orderEntity']->field_redirect_url->uri;
    $response = new TrustedRedirectResponse($redirect_url);
    $form_state->setResponse($response);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $channel = $form_state->getValue('channel');
    $form['#orderEntity']->field_channel = $channel;
    $form['#orderEntity']->save();
    if ($channel == "Alipay") {
      //调起支付宝付款
      $response = \Drupal::service('yunke_order.pay.alipay')->order($form['#orderEntity']);
      $form_state->setResponse($response);
    }
    elseif ($channel == "Wechat") {
      //调起微信支付
      $response = \Drupal::service('yunke_order.pay.wechat')->order($form['#orderEntity']);
      $form_state->setResponse($response);
    }

  }

}
