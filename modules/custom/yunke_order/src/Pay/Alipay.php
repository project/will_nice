<?php
/**
 * 下单对接支付宝接口
 * User: yunke
 * Email: phpworld@qq.com
 * Date: 2021/5/24
 */

namespace Drupal\yunke_order\Pay;

use \Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use \Drupal\node\NodeInterface;


class Alipay {

  //支付宝支付模块接口服务
  protected $alipayAPI;

  //日志记录器
  protected $logger;


  public function __construct($alipayAPI, LoggerChannelFactoryInterface $channelFactory) {
    $this->alipayAPI = $alipayAPI;
    $this->logger = $channelFactory->get('yunke_order');
  }


  /**
   * 调用支付宝接口进行付款操作
   *
   * @param \Drupal\node\NodeInterface $orderEntity
   *
   * @return \Symfony\Component\HttpFoundation\Response  响应对象
   */
  public function order(NodeInterface $orderEntity) {
    $order['order_number'] = $orderEntity->title->value; //订单号
    $order['description'] = $orderEntity->field_description->value; //订单商品或服务的描述
    $order['total'] = (float) ($orderEntity->field_total->value / 100); //订单付款金额 变换单位，又分到元
    $order['timeout_express'] = $orderEntity->field_timeout_express->value; //交易关闭时间

    //准备返回链接
    $route_parameters = [
      'order' => $orderEntity->title->value,
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $return_url = new Url('yunke_order.pay', $route_parameters, $options);
    $order['return_url'] = $return_url->toString(FALSE);


    //准备异步通知链接
    $route_parameters = [
      'type' => 'alipay',
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $notify_url = new Url('yunke_order.notify', $route_parameters, $options);
    $order['notify_url'] = $notify_url->toString(FALSE);

    //$order['attach'] ='user:3'; //平台不需要向支付渠道传递附加数据
    //可选 附加数据 string[1,128] 在查询API和支付通知中原样返回，可作为自定义参数使用
    return $this->alipayAPI->order($order);
  }

  /**
   * 查询订单 视情况更新订单实体
   *
   * @param \Drupal\node\NodeInterface $orderEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function query(NodeInterface $orderEntity) {
    $orderState = $orderEntity->field_order_state->value;
    //查询可能发生在任意时刻 如果是已经付款过的订单 不进行联动查询，以本系统的订单状态为准
    //如果是未付款或付款失败的订单，则向渠道联动查询一次，这可能发生在付款异步通知前进行了主动查询
    if ($orderState != YK_ORDER_STATE_WAIT && $orderState != YK_ORDER_STATE_FALSE) {
      return $orderEntity;
    }
    $orderNumber = $orderEntity->title->value; //以系统订单号进行查询  对于支付宝来说即是商户订单号
    $result = $this->alipayAPI->query($orderNumber);
    if ($result === FALSE) {
      return $orderEntity;
    }
    $data = print_r($result, TRUE);
    $orderEntity->field_channel_data->value = $data;
    $orderEntity->field_notify_state->value = YK_ORDER_NOTICE_CHECKED;
    //标记已收到异步通知 0|尚未处理、1|已收通知、2|已主动查询

    if (!isset($result['code']) || $result['code'] != 10000) {
      //业务层面订单查询异常  可能是订单不存在
      $message = '支付宝订单查询异常，返回值：' . $data;
      $this->logger->warning($message);
      $orderEntity->save();
      return $orderEntity;
    }

    //查询成功
    $orderEntity->field_channel->value = 'Alipay'; //保存付款渠道
    if (isset($result['trade_no'])) {
      $orderEntity->field_channel_order->value = $result['trade_no']; //保存渠道订单号
    }
    if (isset($result['buyer_user_id'])) {
      $orderEntity->field_buyer_id->value = $result['buyer_user_id']; //保存买家ID
    }
    if (isset($result['send_pay_date'])) {
      $orderEntity->field_payment_time->value = strtotime($result['send_pay_date']);//保存付款时间
    }
    else {
      $orderEntity->field_payment_time->value = time();
    }

    //保存付款状态，0|等待付款、1|已经付款、2|付款失败、3|等待退款、4|部分退款、5|全部退款
    if (!isset($result['trade_status'])) {
      //没有状态则默认为等待付款
      $orderEntity->field_order_state->value = YK_ORDER_STATE_WAIT;
    }
    elseif ($result['trade_status'] === 'WAIT_BUYER_PAY') {
      //交易创建，等待买家付款
      $orderEntity->field_order_state->value = YK_ORDER_STATE_WAIT;
    }
    elseif ($result['trade_status'] === 'TRADE_CLOSED') {
      //未付款交易超时关闭，或支付完成后全额退款
      if ($orderEntity->field_order_state->value == YK_ORDER_STATE_WAIT) { //不要破坏退款状态
        $orderEntity->field_order_state->value = YK_ORDER_STATE_FALSE;
      }
      $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_DISABLE;
    }
    elseif ($result['trade_status'] === 'TRADE_FINISHED') {
      //交易结束，不可退款
      $orderEntity->field_order_state->value = YK_ORDER_STATE_SUCCESS;
      $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_DISABLE;
      $orderEntity->field_amount->value = $orderEntity->field_total->value; //实收款
    }
    elseif ($result['trade_status'] === 'TRADE_SUCCESS') {
      //交易支付成功
      $orderEntity->field_order_state->value = YK_ORDER_STATE_SUCCESS;
      $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_ALLOW;
      $orderEntity->field_amount->value = $orderEntity->field_total->value; //实收款
    }
    //实测发现订单金额为几分钱时，仅$result['total_amount']参数正确表示，其他金额参数均为0，因此不进行金额判断
    $orderEntity->save();
    return $orderEntity;
  }

  /**
   * 执行退款查询并更新退款实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function queryRefund(NodeInterface $refundEntity) {
    $refundState = $refundEntity->field_refund_state->value;
    //查询可能发生在任意时刻 如果是已经付款过的订单 不进行联动查询，以本系统的订单状态为准
    //如果是未付款或付款失败的订单，则向渠道联动查询一次，这可能发生在付款异步通知前进行了主动查询
    if ($refundState != YK_ORDER_REFUND_STATE_WAIT && $refundState != YK_ORDER_REFUND_STATE_FALSE) {
      return $refundEntity;
    }
    $orderNumber = $refundEntity->title->value; //以系统订单号进行查询  对于支付宝来说即是商户订单号
    $refundNumber = $refundEntity->field_refund_number->value;
    $result = $this->alipayAPI->queryRefund($orderNumber, $refundNumber);
    if ($result === FALSE) {
      return $refundEntity;
    }
    $data = print_r($result, TRUE);
    $refundEntity->field_channel_data->value = $data;
    if (!isset($result['code']) || $result['code'] != 10000) {
      //业务层面订单退款查询异常  可能是退款单不存在
      $message = '支付宝订单退款异常，返回值：' . $data;
      $this->logger->warning($message);
      $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
      return $refundEntity;
    }
    //trade_no支付宝交易号、out_request_no退款单号、refund_amount退款金额不为空，表示退款成功
    if (
      !empty($result['trade_no'])
      && !empty($result['out_request_no']) && $result['out_request_no'] == $refundEntity->field_refund_number->value
      && !empty($result['refund_amount']) && (int) ($result['refund_amount'] * 100) == (int) $refundEntity->field_refund_amount->value
    ) {
      //退款成功
      $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_SUCCESS;
      if (!empty($result['gmt_refund_pay'])) {
        $refundEntity->field_success_time->value = strtotime($result['gmt_refund_pay']);
      }
      else {
        $refundEntity->field_success_time->value = time();
      }
    }
    else {
      //退款失败
      $message = '支付宝订单退款异常，返回值：' . $data;
      $this->logger->warning($message);
      $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
    }
    return $refundEntity;
  }

  /**
   * 向支付宝发起退款并更新退款实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function refund(NodeInterface $refundEntity) {
    $order['out_trade_no'] = $refundEntity->title->value;//必选 商户订单号
    $order['refund_amount'] = (float) ($refundEntity->field_refund_amount->value / 100); //必选 退款金额 单位元，可精确到两位小数
    $order['refund_reason'] = $refundEntity->field_refund_reason->value;//可选 退款原因 String  最大长度256
    $order['out_request_no'] = $refundEntity->field_refund_number->value; //按条件可选 退款单号  String  最大长度64
    $result = $this->alipayAPI->refund($order);
    if ($result === FALSE) {
      return $refundEntity;
    }
    $data = print_r($result, TRUE);
    if (!isset($result['code']) || $result['code'] != 10000) {
      //业务层面订单查询异常  可能是订单不存在
      $message = '支付宝订单退款异常，返回值：' . $data;
      $this->logger->warning($message);
      $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
      $refundEntity->field_channel_data->value = $data;
      return $refundEntity;
    }
    if (isset($result['fund_change']) && $result['fund_change'] == 'Y') {
      //退款成功
      $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_SUCCESS;
      $refundEntity->field_channel_data->value = $data;
      if (!empty($result['gmt_refund_pay'])) {
        $refundEntity->field_success_time->value = strtotime($result['gmt_refund_pay']);
      }
    }
    return $refundEntity;
  }

}
