<?php
/**
 * 下单对接微信支付接口
 * User: yunke
 * Email: phpworld@qq.com
 * Date: 2021/5/24
 */

namespace Drupal\yunke_order\Pay;

use \Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use \Drupal\node\NodeInterface;
use \Symfony\Component\HttpFoundation\Response;
use \Drupal\Core\Routing\TrustedRedirectResponse;


class Wechat {

  //微信支付模块接口服务
  protected $wechatAPI;

  //日志记录器
  protected $logger;


  public function __construct($wechatAPI, LoggerChannelFactoryInterface $channelFactory) {
    $this->wechatAPI = $wechatAPI;
    $this->logger = $channelFactory->get('yunke_order');
  }

  /**
   * 调用微信接口进行付款操作
   *
   * @param \Drupal\node\NodeInterface $orderEntity
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   */
  public function order(NodeInterface $orderEntity) {
    $order['order_number'] = $orderEntity->title->value; //订单号
    $order['description'] = $orderEntity->field_description->value; //订单商品或服务的描述
    $order['total'] = (int) $orderEntity->field_total->value; //订单付款金额 微信的单位为分
    $order['timeout_express'] = $orderEntity->field_timeout_express->value; //交易关闭时间

    //微信的返回链接设置和支付宝不同，微信默认返回发起支付的页面，也可在中间调转页面拼接redirect_url参数指定
    //准备返回链接
    $route_parameters = [
      'order' => $orderEntity->title->value,
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $return_url = new Url('yunke_order.pay', $route_parameters, $options);
    $order['return_url'] = $return_url->toString(FALSE);

    // 准备异步通知链接
    $route_parameters = [
      'type' => 'wechat',
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $notify_url = new Url('yunke_order.notify', $route_parameters, $options);
    $order['notify_url'] = $notify_url->toString(FALSE);
    $response = $this->wechatAPI->order($order);
    if ($response instanceof Response) {
      return $response;
    }

    //通讯失败 返回页面并给出提示
    $message = '微信支付异常:' . $response;
    \Drupal::messenger()->addError($message); //显示消息给用户
    $this->logger->error($message);
    $route_parameters = ['order' => $orderEntity->title->value,];
    $options = ['absolute' => TRUE,];
    $return_url = new Url('yunke_order.pay', $route_parameters, $options);
    return new TrustedRedirectResponse($return_url->toString(FALSE));
  }

  /**
   * 查询订单 视情况更新订单实体
   *
   * @param \Drupal\node\NodeInterface $orderEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function query(NodeInterface $orderEntity) {
    $orderState = $orderEntity->field_order_state->value;
    //查询可能发生在任意时刻 如果是已经付款过的订单 不进行联动查询，以本系统的订单状态为准
    //如果是未付款或付款失败的订单，则向渠道联动查询一次，这可能发生在付款异步通知前进行了主动查询
    if ($orderState != YK_ORDER_STATE_WAIT && $orderState != YK_ORDER_STATE_FALSE) {
      return $orderEntity;
    }
    $orderNumber = $orderEntity->title->value; //以系统订单号进行查询  对于微信支付来说即是商户订单号
    $result = $this->wechatAPI->query($orderNumber);
    if ($result === FALSE) {
      return $orderEntity;
    }
    $data = print_r($result, TRUE);
    $orderEntity->field_channel_data->value = $data;
    $orderEntity->field_notify_state->value = YK_ORDER_NOTICE_CHECKED;
    //标记已收到异步通知 0|尚未处理、1|已收通知、2|已主动查询

    if (!isset($result['trade_state']) || $result['trade_state'] != 'SUCCESS') {
      //这里均被认为是失败
      if (isset($result['trade_state']) && $result['trade_state'] == 'PAYERROR') {
        $orderEntity->field_order_state->value = YK_ORDER_STATE_FALSE;
      }
      $orderEntity->save();
      return $orderEntity;
    }

    //查询成功
    $orderEntity->field_channel->value = 'Wechat'; //保存付款渠道
    if (isset($result['transaction_id'])) {
      $orderEntity->field_channel_order->value = $result['transaction_id']; //保存渠道订单号
    }
    if (isset($result['payer']->openid)) {
      $orderEntity->field_buyer_id->value = $result['payer']->openid; //保存买家ID
    }
    if (isset($result['success_time'])) {
      $orderEntity->field_payment_time->value = strtotime($result['success_time']);//保存付款时间
    }
    else {
      $orderEntity->field_payment_time->value = time();
    }

    //交易支付成功
    $orderEntity->field_order_state->value = YK_ORDER_STATE_SUCCESS;
    $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_ALLOW;
    $orderEntity->field_amount->value = $orderEntity->field_total->value; //实收款

    //付款金额：$result['amount']->payer_total，
    //总金额：$result['amount']->total，不进行金额判断
    $orderEntity->save();
    return $orderEntity;
  }

  /**
   * 执行退款 并更新退款实体 但不保存实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function refund(NodeInterface $refundEntity) {
    $order['order_number'] = $refundEntity->title->value;//必选 商户订单号
    $order['refund_amount'] = $refundEntity->field_refund_amount->value; //必选 退款金额 单位分
    $order['total'] = $refundEntity->field_total->value; //必选 原订单总金额 单位分
    $order['refund_reason'] = $refundEntity->field_refund_reason->value;//可选 退款原因 String  最大长度256
    $order['refund_number'] = $refundEntity->field_refund_number->value; //必选 退款单号  String  最大长度64
    $order['refund_number'] = $order['order_number'] . $order['refund_number']; //腾讯要求退款单号全局唯一，而不是订单号下唯一

    //退款异步通知地址
    $route_parameters = [
      'type' => 'wechat',
    ];
    $options = [
      'absolute' => TRUE,
    ];
    $notify_url = new Url('yunke_order.notify.refund', $route_parameters, $options);
    $order['notify_url'] = $notify_url->toString(FALSE);
    $result = $this->wechatAPI->refund($order);
    if ($result === FALSE) {
      return $refundEntity;
    }
    $data = print_r($result, TRUE);
    if (!isset($result['status']) || $result['status'] != 'SUCCESS') {
      if (isset($result['status']) && $result['status'] != "PROCESSING") {//说明退款关闭或异常 CLOSED、ABNORMAL
        $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
      }
      $refundEntity->field_channel_data->value = $data;
      return $refundEntity;
    }

    //退款成功
    $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_SUCCESS;
    $refundEntity->field_channel_data->value = $data;
    if (!empty($result['success_time'])) {
      $refundEntity->field_success_time->value = strtotime($result['success_time']);
    }
    return $refundEntity;
  }

  /**
   * 查询退款状态 并更新退款实体，但不保存实体
   *
   * @param \Drupal\node\NodeInterface $refundEntity
   *
   * @return \Drupal\node\NodeInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function queryRefund(NodeInterface $refundEntity) {
    $refundState = $refundEntity->field_refund_state->value;
    //查询可能发生在任意时刻 如果是已经付款过的订单 不进行联动查询，以本系统的订单状态为准
    //如果是未付款或付款失败的订单，则向渠道联动查询一次，这可能发生在付款异步通知前进行了主动查询
    if ($refundState != YK_ORDER_REFUND_STATE_WAIT && $refundState != YK_ORDER_REFUND_STATE_FALSE) {
      return $refundEntity;
    }
    $orderNumber = $refundEntity->title->value; //以系统订单号进行查询  对于支付宝来说即是商户订单号
    $refundNumber = $refundEntity->field_refund_number->value;

    //微信支付退款单号要求全局唯一，而不是订单号下唯一，因此需要进行转化处理
    $outRefundNo = $orderNumber . $refundNumber;

    $result = $this->wechatAPI->queryRefund($outRefundNo);
    if ($result === FALSE) {
      return $refundEntity;
    }

    $data = print_r($result, TRUE);
    $refundEntity->field_channel_data->value = $data;
    if (!isset($result['status']) || $result['status'] != 'SUCCESS') {
      if (isset($result['status']) && $result['status'] != "PROCESSING") {//说明退款关闭或异常 CLOSED、ABNORMAL
        $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_FALSE;
      }
      return $refundEntity;
    }

    //退款成功
    $refundEntity->field_refund_state->value = YK_ORDER_REFUND_STATE_SUCCESS;
    if (!empty($result['success_time'])) {
      $refundEntity->field_success_time->value = strtotime($result['success_time']);
    }
    return $refundEntity;
  }

}
