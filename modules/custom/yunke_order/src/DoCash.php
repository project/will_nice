<?php

/**
 *  通过批处理执行提现操作
 *  by:yunke
 *  email:phpworld@qq.com
 */

namespace Drupal\yunke_order;

class DoCash {

  /**
   * 提现批处理回调
   *
   * @param $arguments
   * @param $context
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function cash($arguments, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['count'] = 0;
      $context['results']['cashEntityID'] = $arguments['cashEntityID'];
    }
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $cashEntity = $storager->load($arguments['cashEntityID']);
    $ids = $storager->getQuery('AND')
      ->condition("type", "order", '=')
      ->condition("field_user_id", $arguments['userID'], '=')
      ->condition("field_order_state", [YK_ORDER_STATE_SUCCESS, YK_ORDER_STATE_REFUND_PART,], 'IN')
      //仅含付款成功和部分退款订单，不包含正在退款中的订单（其可能导致金额低于阈值），也不包含完整退款的订单（其金额为0）
      ->condition("field_cash", FALSE, '=')
      //仅包含未提现订单，已提现的订单不能再提
      ->condition("field_payment_time", $arguments['timeLimit'], '<')
      //仅包含成功付款在当天前的订单 因为支付宝和微信都是T+1到账
      ->sort('field_payment_time', 'ASC') //ASC DESC
      ->range(0, $arguments['cashBatchLimit'])
      ->execute();
    $orderEntities = $storager->loadMultiple($ids);
    foreach ($orderEntities as $orderEntity) {
      $cashEntity->field_amount->value += $orderEntity->field_amount->value;
      $orderEntity->field_cash->value = TRUE; //设置已经提现
      $orderEntity->field_refund_disable->value = YK_ORDER_REFUND_DISABLE; //设置禁止退款
    }

    $con = \Drupal::database();
    $yunke = $con->startTransaction();
    //开启数据库事务，参数指定回滚点，只要变量$yunke不被销毁那么事务持续开启，一旦销毁即被提交，
    //原理是事务对象失去变量引用时，被php销毁，执行了析构函数。
    $count = $context['sandbox']['count']; //保存状态 这允许发生异常时重试
    $results = $context['results'];
    try {
      $cashEntity->save();
      foreach ($orderEntities as $orderEntity) {
        $orderEntity->save();
        $context['sandbox']['count']++;
        $context['results'][] = $orderEntity->id();
      }
    } catch (\Exception $e) {
      $context['sandbox']['count'] = $count;
      $context['results'] = $results;
      //这可能导致部分订单提现失败，但数据不会发生错误，它们会在下一次提现中提取
      $msg = "用户" . $arguments['userID'] . '提现处理时实体保存异常，数据库已回滚，提现单号：' . $cashEntity->id();
      $msg .= ",异常消息：" . $e->getMessage();
      \Drupal::logger('yunke_order')->error($msg);
      $con->rollback($yunke->name());  //回滚事务到回滚点
    }
    unset($yunke);//变量被销毁，事务被提交
    //不要使用$con->commit();去提交一个事务，这会抛出异常，系统会隐式自动提交

    $context['finished'] = $context['sandbox']['count'] / $arguments['total'];
    $context['message'] = '已处理订单：' . $context['sandbox']['count'] . "个，共{$arguments['total']}个";
  }

  /**
   * 批处理完成回调  计算应付款和标记提现申请
   *
   * @param $success
   * @param $results
   * @param $operations
   * @param $elapsed
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function cashFinished($success, $results, $operations, $elapsed) {
    $messenger = \Drupal::messenger();
    $cashEntityID = $results['cashEntityID'];
    unset($results['cashEntityID']);
    //不论是否提现完全成功 都应该更新提现实体，因为订单提现状态是不可逆转的，可能有部分已经被提现了
    //现在计算应付款
    $storager = \Drupal::entityTypeManager()->getStorage("node");
    $cashEntity = $storager->load($cashEntityID);
    $cost = round($cashEntity->field_amount->value * $cashEntity->field_cash_rate->value); //平台营运扣费
    $cashEntity->field_cash_amount->value = $cashEntity->field_amount->value - $cost;
    $cashEntity->field_cash_state->value = YK_ORDER_CASH_STATE_SUCCESS;

    if ($success) {
      $cashEntity->field_cash_msg->value = '本单已全部成功提现，等待转账';
      $message = '提现已成功执行，所提现订单数量：' . count($results) . '， 耗时：' . $elapsed;
      $messenger->addStatus($message);
    }
    else {
      $cashEntity->field_cash_msg->value = '本单已部分提现，等待转账，未提现部分将在下一次申请中一并提现';
      $message = '提现失败，或未全部提现，但已提现订单仍应被付款';
      $messenger->addWarning($message);
    }
    $cashEntity->save();
  }


}
