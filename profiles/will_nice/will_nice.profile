<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function will_nice_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['#submit'][] = 'will_nice_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function will_nice_form_install_configure_submit($form, FormStateInterface $form_state) {
  $site_mail = $form_state->getValue('site_mail');
  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}

/**
 * 使得用户协议成为第一个安装任务
 *
 * @param $tasks
 * @param $install_state
 */
function will_nice_install_tasks_alter(&$tasks, $install_state) {
  $firstTask = ['will_nice_protocol' => $tasks['will_nice_protocol']];
  unset($tasks['will_nice_protocol']);
  $tasks = $firstTask + $tasks;
}

